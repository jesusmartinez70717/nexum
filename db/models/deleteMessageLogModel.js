const mongoDb = require('../mongoDb.js')

const Model = mongoDb.createModel('deletemsglog430838724', {
    Guild: String,
    Channel: String
})

module.exports = Model