const mongoDb = require('../mongoDb.js')

const Model = mongoDb.createModel('warnings', {
    GuildID: String,
    UserID: String,
    UserTag: String,
    Content: Array
})

module.exports = Model