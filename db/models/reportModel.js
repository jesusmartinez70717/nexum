const mongoDb = require('../mongoDb.js')

const Model = mongoDb.createModel('report', {
    Guild: String,
    Channel: String,
    Msg: String,
    Role: String
})

module.exports = Model