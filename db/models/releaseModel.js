const mongoDb = require('../mongoDb.js')

const Model = mongoDb.createModel('releasesnotes34938248', {
    Updates: String,
    Date: String,
    Developer: String,
    Version: Number
})

module.exports = Model