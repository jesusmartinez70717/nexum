const mongoDb = require('../../mongoDb.js');
const mongoose = require('mongoose');

const dragonSchema = new mongoDb.createModel('dragons34428033', {
    name: { type: String, required: true },
    type: { type: String, required: true },
    level: { type: Number, default: 1 },
    owner: { type: mongoose.Schema.Types.ObjectId, ref: 'profiles5t850586' },
    isForSale: { type: Boolean, default: false },
    price: { type: Number, default: 0 },
    attack: { type: Number, required: true, default: 10 },
    defense: { type: Number, required: true, default: 10 },
    speed: { type: Number, required: true, default: 10 },
    hp: { type: Number, required: true, default: 100 },
    uniqueName: { type: String, required: true },
    abilities: { type: [String], required: true },
    rarity: { type: String, required: true, default: 'común' }
});

module.exports = dragonSchema;
