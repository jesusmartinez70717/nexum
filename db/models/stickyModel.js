const mongoDb = require('../mongoDb.js')

const Model = mongoDb.createModel('sticky34347320', {
    Guild: String,
    Channel: String,
    Message: String,
    Count: Number,
    Cap: Number
})

module.exports = Model