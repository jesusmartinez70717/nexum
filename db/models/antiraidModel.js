const mongoDb = require('../mongoDb.js'); // Asegúrate de tener configurada la conexión a MongoDB aquí

// Definir el Schema para la configuración de AntiRaid
const antiRaidConfigSchema = mongoDb.createModel('antiraid3493402', {
  guildId: {type: String, required: true, unique: true },
  detectarMensajes: { type: Boolean, required: true },
  detectarCrearCanal: { type: Boolean, required: true },
  accion: { type: String, enum: ['warn', 'mute', 'kick', 'ban', 'none'], required: true },
  mensajes: { type: Number, required: true },
  canalLogsId: { type: String, required: true }
});

module.exports = antiRaidConfigSchema