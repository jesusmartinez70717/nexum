const mongoDb = require('../../mongoDb.js')
const mongoose = require('mongoose')
const Model = mongoDb.createModel('profiles5t850586', {
    userId: { type: String, required: true, unique: true },
    serverId: { type: String, required: true },
    balance: { type: Number, default: 500 },
    dailyLastUsed: { type: Number, default: 0 },
    coinflipLastUsed: { type: Number, default: 0 },
    inventory: {
        dragons: [{ type: mongoose.Schema.Types.ObjectId, ref: 'dragons34428033' }]
    }
});

module.exports = Model;
