const mongoose = require('mongoose');
const logger = require('../infrastructure/logging/logger.js');
require('dotenv').config();

// Conexión a la base de datos MongoDB
async function connectDB() {
    try {
        await mongoose.connect(process.env.MONGDB_URL);
        logger.info('mongoDb', 'MongoDB conectado');
    } catch (error) {
        logger.error('mongoDb', 'Error al conectar a MongoDB:', error);
        throw error;
    }
}

// Función para definir modelos
function createModel(name, schemaDefinition) {
    const schema = new mongoose.Schema(schemaDefinition, { timestamps: true });
    return mongoose.model(name, schema);
}

// Funciones CRUD y adicionales
async function getData(model, query = {}) {
    try {
        const documents = await model.find(query);
        return documents;
    } catch (error) {
        logger.error('mongoDb', 'Error al obtener datos:', error);
        throw error;
    }
}

async function queryData(model, field, value) {
    try {
        const documents = await model.find({ [field]: value });
        return documents;
    } catch (error) {
        logger.error('mongoDb', 'Error al realizar la consulta:', error);
        throw error;
    }
}

async function setData(model, data) {
    try {
        const document = new model(data);
        await document.save();
        logger.info('mongoDb', 'Datos guardados exitosamente.');
        return document;
    } catch (error) {
        logger.error('mongoDb', 'Error al guardar datos:', error);
        throw error;
    }
}

async function updateData(model, id, data) {
    try {
        const document = await model.findByIdAndUpdate(id, data, { new: true });
        logger.info('mongoDb', 'Datos actualizados exitosamente.');
        return document;
    } catch (error) {
        logger.error('mongoDb', 'Error al actualizar datos:', error);
        throw error;
    }
}

async function deleteData(model, id) {
    try {
        await model.findByIdAndDelete(id);
        logger.info('mongoDb', 'Datos borrados exitosamente.');
    } catch (error) {
        logger.error('mongoDb', 'Error al borrar datos:', error);
        throw error;
    }
}

async function deleteSpecificData(model, field, value) {
    try {
        const result = await model.deleteMany({ [field]: value });
        if (result.deletedCount > 0) {
            logger.info('mongoDb', 'Datos eliminados correctamente.');
        } else {
            logger.info('mongoDb', 'No se encontraron datos para eliminar.');
        }
    } catch (error) {
        logger.error('mongoDb', 'Error al eliminar datos:', error);
        throw error;
    }
}

module.exports = {
    connectDB,
    createModel,
    getData,
    setData,
    updateData,
    deleteData,
    deleteSpecificData,
    queryData,
};