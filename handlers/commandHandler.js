const { Collection, EmbedBuilder } = require('discord.js');
const path = require('node:path');
const fs = require('node:fs');

class CommandHandler {
  constructor(prefix) {
    this.commands = new Collection();
    this.prefix = prefix;
    this.botOwnerId = process.env.BOT_OWNER_ID; // Asegúrate de tener esta variable en tu archivo de entorno
  }

  addCommand(command) {
    if (command.owner === undefined) {
      command.owner = false;
    }
    this.commands.set(command.name, command);
  }

  async executeCommand(client, message) {
    const args = message.content.slice(this.prefix.length).trim().split(/ +/);
    const commandName = args.shift().toLowerCase();
    const command = this.commands.get(commandName);

    if (!command) {
      const embed = new EmbedBuilder()
        .setColor('#FF0000')
        .setTitle('Error')
        .setDescription(`El comando \`${commandName}\` no existe.`)
        .setTimestamp();
      message.channel.send({ embeds: [embed] });
      return;
    }

    if (command.owner && message.author.id !== this.botOwnerId) {
      const embed = new MessageEmbed()
        .setColor('#FF0000')
        .setTitle('Permiso Denegado')
        .setDescription('Este comando solo puede ser usado por el propietario del bot.')
        .setTimestamp();
      message.channel.send({ embeds: [embed] });
      return;
    }

    try {
      await command.execute(message, args, client);
    } catch (error) {
      console.error(`Error ejecutando el comando ${commandName}:`, error);
    }
  }

  loadCommandsFromDirectory(commandsPath) {
    const absolutePath = path.resolve(process.cwd(), commandsPath);
    this.loadCommandsRecursively(absolutePath);
  }

  loadCommandsRecursively(directory) {
    const commandFiles = fs.readdirSync(directory);
    for (const file of commandFiles) {
      const filePath = path.join(directory, file);
      const stats = fs.statSync(filePath);

      if (stats.isDirectory()) {
        this.loadCommandsRecursively(filePath);
      } else if (stats.isFile() && file.endsWith('.js')) {
        const command = require(filePath);
        if (command.name) {
          this.addCommand(command);
          console.log(`Comando ${command.name} cargado`);
        } else {
          console.error(`El comando en el archivo ${file} no tiene una propiedad 'name'.`);
        }
      }
    }
  }
}

module.exports = CommandHandler;
