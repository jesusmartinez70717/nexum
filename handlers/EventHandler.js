const { readdirSync } = require('node:fs');
const { join, resolve } = require('node:path');
const logger = require('../infrastructure/logging/logger');

function loadEvents(client, eventsPath, showLoadedMessage = true) {
  if (!eventsPath) {
    logger.error('EventHandler', 'Debe proporcionar una ruta válida para cargar los eventos.');
    return;
  }

  const absolutePath = resolve(eventsPath);
  const eventFiles = readdirSync(absolutePath).filter(file => file.endsWith('.js'));

  for (const file of eventFiles) {
    const eventModule = require(join(absolutePath, file));

    if (!eventModule.name || typeof eventModule.execute !== 'function') {
      logger.error('EventHandler', `El archivo ${file} no exporta correctamente un evento. Se requiere una propiedad 'name' y un método 'execute'.`);
      continue;
    }

    // Registrar el evento pasando el cliente como primer argumento
    client.on(eventModule.name, eventModule.execute.bind(null, client));
    delete require.cache[require.resolve(join(absolutePath, file))];

    if (showLoadedMessage) {
      logger.success('EventHandler', `Evento ${eventModule.name} cargado`);
    }
  }
};


module.exports = { loadEvents };
