const { Collection, EmbedBuilder } = require('discord.js');
const profileSchema = require('../db/models/economy/profileModel.js')
const path = require('path');
const fs = require('fs');
const logger = require('../infrastructure/logging/logger.js');

class SlashCommandHandler {
  constructor(client, commandsPath) {
    if (!commandsPath) {
      logger.error('SlashCommandHandler', 'Debe proporcionar una ruta válida para cargar los slash commands.');
      return;
    }

    this.client = client;
    this.commandsPath = commandsPath;
    this.commands = new Collection();
    this.client.commands = this.commands;
    this.botOwnerId = process.env.BOT_OWNER_ID; // Asegúrate de tener esta variable en tu archivo de entorno
  }

  async loadSlashCommands() {
    const absolutePath = path.resolve(this.commandsPath);
    await this.loadCommandsRecursive(absolutePath);
  }

  async eliminarTodosSlashCommands() {
    try {
      // Obtener todos los comandos slash registrados en Discord
      const comandosRegistrados = await this.client.application.commands.fetch();

      // Eliminar cada comando slash de Discord y de la colección
      for (const command of comandosRegistrados.values()) {
        await this.client.application.commands.delete(command.id);
        this.commands.delete(command.id);
        console.log(`${command.name} fue eliminado`)
      }

      console.log('Todos los comandos slash han sido eliminados.');
    } catch (error) {
      console.error('Error al eliminar los comandos slash:', error);
    }
  }

  async loadCommandsRecursive(dir) {
    const commandFiles = fs.readdirSync(dir);

    for (const file of commandFiles) {
      const filePath = path.join(dir, file);
      const stat = fs.lstatSync(filePath);

      if (stat.isDirectory()) {
        await this.loadCommandsRecursive(filePath); // Recursively load commands in subdirectories
      } else if (file.endsWith('.js')) {
        try {
          const slashCommand = require(filePath);
          if (slashCommand.owner === undefined) {
            slashCommand.owner = false;
          }
          const command = await this.client.application.commands.create(slashCommand.data);
          this.commands.set(command.id, slashCommand);
          logger.success('SlashCommandHandler', `Comando ${command.name} cargado y registrado como Slash Command`);
        } catch (error) {
  const commandName = file.replace('.js', ''); // Extraer el nombre del archivo sin la extensión
          logger.error('SlashCommandHandler', `Error al cargar el comando de Slash Command "${commandName}": ${error}`);
        }
      }
    }
  }

  listenForInteractions() {
    this.client.on('interactionCreate', async (interaction) => {
      let profileData;
      try {
          profileData = await profileSchema.findOne({ userId: interaction.user.id})
          if(!profileData) {
              profileData = await profileSchema.create({
                  userId: interaction.user.id,
                  serverId: interaction.guild.id
              })
          }
      } catch (error) {
          
      }
      if (!interaction.isCommand()) return;

      const command = this.commands.find(c => c.data.name === interaction.commandName);
      if (!command) {
        const embed = new EmbedBuilder()
          .setColor('#FF0000')
          .setTitle('Error')
          .setDescription(`El comando \`${interaction.commandName}\` no está registrado.`)
          .setTimestamp();
        await interaction.reply({ embeds: [embed], ephemeral: true });
        logger.error('SlashCommandHandler', `El comando slash "${interaction.commandName}" no está registrado.`);
        return;
      }

      if (command.owner && interaction.user.id !== this.botOwnerId) {
        const embed = new EmbedBuilder()
          .setColor('#FF0000')
          .setTitle('Permiso Denegado')
          .setDescription('Este comando solo puede ser usado por el propietario del bot.')
          .setTimestamp();
        await interaction.reply({ embeds: [embed], ephemeral: true });
        return;
      }

      try {
        await command.execute(this.client, interaction, profileData);
      } catch (error) {
        logger.error('SlashCommandHandler', `Error al ejecutar el comando slash "${interaction.commandName}": ${error.stack}`, );
      }
    });
  }
}

module.exports = SlashCommandHandler;
