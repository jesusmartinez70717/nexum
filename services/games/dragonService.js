// dragonService.js

const Dragon = require('../../db/models/dragonModel');
const dragonsData = require('../../data/dragons.json');

async function createDragons() {
    try {
        // Verificar si ya hay dragones en la base de datos
        const count = await Dragon.countDocuments();
        if (count > 0) {
            console.log('Ya existen dragones en la base de datos. No se crearán más.');
            return;
        }

        // Crear los dragones iniciales desde el archivo dragons.json
        await Dragon.insertMany(dragonsData);
        console.log('Dragones creados exitosamente.');
    } catch (error) {
        console.error('Error al crear dragones:', error);
        throw error;
    }
}

async function getAllDragons() {
    try {
        // Obtener todos los dragones
        const dragons = await Dragon.find();
        return dragons;
    } catch (error) {
        console.error('Error al obtener dragones:', error);
        throw error;
    }
}

async function getDragonById(dragonId) {
    try {
        // Obtener un dragón por su ID
        const dragon = await Dragon.findById(dragonId);
        if (!dragon) {
            throw new Error('Dragón no encontrado');
        }
        return dragon;
    } catch (error) {
        console.error('Error al obtener dragón por ID:', error);
        throw error;
    }
}

async function updateDragon(dragonId, updatedDragon) {
    try {
        // Actualizar un dragón por su ID
        const dragon = await Dragon.findByIdAndUpdate(dragonId, updatedDragon, { new: true });
        if (!dragon) {
            throw new Error('Dragón no encontrado');
        }
        return dragon;
    } catch (error) {
        console.error('Error al actualizar dragón:', error);
        throw error;
    }
}

async function deleteDragon(dragonId) {
    try {
        // Eliminar un dragón por su ID
        const dragon = await Dragon.findByIdAndDelete(dragonId);
        if (!dragon) {
            throw new Error('Dragón no encontrado');
        }
        return dragon;
    } catch (error) {
        console.error('Error al eliminar dragón:', error);
        throw error;
    }
}

module.exports = {
    createDragons,
    getAllDragons,
    getDragonById,
    updateDragon,
    deleteDragon
};
