const log = require('../../db/models/deleteMessageLogModel.js')
const { EmbedBuilder } = require('discord.js')
async function DeleteMessageSystem(client, message) {
if(!message.guild || !message.author || message.author.bot || !message) return;

let data = await log.findOne({Guild: message.guild.id})
if(!data) return;
let sendChannel = await message.guild.channels.fetch(data.Channel);
let attachments = await message.attachments.map(attachment => attachment.url);
let member = message.author;
let deleteTimme = `<t:${Math.floor(Date.now() / 1000)}:R>`

const embed = new EmbedBuilder()
.setColor('Red')
.setTitle('Nuevo mensaje eliminado')
.setDescription(`Este mensaje se elimino ${deleteTimme} y se esta registrando con fines de moderacion`)
.addFields(
    { name: 'Autor', value: `> \`${member.username} (${member.id})\``},
    { name: 'Contenido', value: `> ${message.content || 'No hay contenido en el mensaje'}` },
    { name: 'Canal del mensaje', value: `> ${message.channel} (${message.channel.id})`}
)
.setFooter({text: 'Sistema de registro de mensajes eliminados'})
.setTimestamp()

if(attachments.length > 0) {
    embed.addFields({name: 'Adjuntos del mensaje', value: attachments.join(' , ')})
}

await sendChannel.send({embeds: [embed]})
}

module.exports = DeleteMessageSystem