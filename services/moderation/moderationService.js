// moderationService.js

const Profile = require('../../db/models/profileModel');
const Warning = require('../../db/models/warningModel');
const { getUserFromMention } = require('../utilities/validationUtils');
const { sendEmbedMessage } = require('../utilities/embedUtils');
const logger = require('../../infrastructure/logging/logger');

async function warnMember(guild, moderator, member, reason) {
    try {
        const profile = await Profile.findOne({ userId: member.id, serverId: guild.id });
        if (!profile) {
            throw new Error('Perfil no encontrado para el miembro.');
        }

        const warning = new Warning({
            userId: member.id,
            serverId: guild.id,
            moderatorId: moderator.id,
            reason: reason
        });

        await warning.save();

        logger.info('moderationService', `Miembro advertido: ${member.user.tag}`);

        const embed = {
            title: 'Miembro Advertido',
            description: `${member.user.tag} ha sido advertido por ${moderator.tag}. Razón: ${reason}`,
            color: 0xff0000
        };

        await sendEmbedMessage(guild, embed);

        return warning;
    } catch (error) {
        logger.error('moderationService', 'Error al advertir miembro:', error);
        throw error;
    }
}

async function getWarningsForMember(member) {
    try {
        const warnings = await Warning.find({ userId: member.id });
        return warnings;
    } catch (error) {
        logger.error('moderationService', 'Error al obtener advertencias:', error);
        throw error;
    }
}

async function clearWarningsForMember(member) {
    try {
        await Warning.deleteMany({ userId: member.id });
        logger.info('moderationService', `Advertencias eliminadas para el miembro: ${member.user.tag}`);
    } catch (error) {
        logger.error('moderationService', 'Error al limpiar advertencias:', error);
        throw error;
    }
}

async function kickMember(guild, moderator, member, reason) {
    try {
        await member.kick(reason);
        logger.info('moderationService', `Miembro expulsado: ${member.user.tag}`);

        const embed = {
            title: 'Miembro Expulsado',
            description: `${member.user.tag} ha sido expulsado por ${moderator.tag}. Razón: ${reason}`,
            color: 'Red'
        };

        await sendEmbedMessage(guild, embed);
    } catch (error) {
        logger.error('moderationService', 'Error al expulsar miembro:', error);
        throw error;
    }
}

async function banMember(guild, moderator, member, reason, days = 0) {
    try {
        await guild.members.ban(member.id, { days: days, reason: reason });
        logger.info('moderationService', `Miembro baneado: ${member.user.tag}`);

        const embed = {
            title: 'Miembro Baneado',
            description: `${member.user.tag} ha sido baneado por ${moderator.tag}. Razón: ${reason}`,
            color: 'Red'
        };

        await sendEmbedMessage(guild, embed);
    } catch (error) {
        logger.error('moderationService', 'Error al banear miembro:', error);
        throw error;
    }
}

async function muteMember(guild, moderator, member, reason, muteRole) {
    try {
        await member.roles.add(muteRole);
        logger.info('moderationService', `Miembro silenciado: ${member.user.tag}`);

        const embed = {
            title: 'Miembro Silenciado',
            description: `${member.user.tag} ha sido silenciado por ${moderator.tag}. Razón: ${reason}`,
            color: 'Red'
        };

        await sendEmbedMessage(guild, embed);
    } catch (error) {
        logger.error('moderationService', 'Error al silenciar miembro:', error);
        throw error;
    }
}

async function unmuteMember(guild, member, muteRole) {
    try {
        await member.roles.remove(muteRole);
        logger.info('moderationService', `Miembro desilenciado: ${member.user.tag}`);

        const embed = {
            title: 'Miembro Desilenciado',
            description: `${member.user.tag} ha sido desilenciado.`,
            color: 'Red'
        };

        await sendEmbedMessage(guild, embed);
    } catch (error) {
        logger.error('moderationService', 'Error al desilenciar miembro:', error);
        throw error;
    }
}

async function purgeMessages(channel, limit) {
    try {
        const fetched = await channel.messages.fetch({ limit: limit });
        await channel.bulkDelete(fetched);
        logger.info('moderationService', `${fetched.size} mensajes eliminados en ${channel.name}`);
    } catch (error) {
        logger.error('moderationService', 'Error al purgar mensajes:', error);
        throw error;
    }
}

module.exports = {
    warnMember,
    getWarningsForMember,
    clearWarningsForMember,
    kickMember,
    banMember,
    muteMember,
    unmuteMember,
    purgeMessages
};
