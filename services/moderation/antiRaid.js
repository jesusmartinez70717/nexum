const { EmbedBuilder } = require('discord.js'); // Asegúrate de importar correctamente EmbedBuilder
// Crear una colección para mantener un registro de usuarios y canales
const usersMassMessage = new Map();
const channelsMassCreate = new Map();
let muteUntil =new Date();
  // Enumeración para las acciones de anti-raid
  const AntiRaidActions = {
    WARN: 'warn',
    MUTE: 'mute',
    KICK: 'kick',
    BAN: 'ban',
    DELETE: 'delete',
    NONE: 'none',
  };

  
function handleMassMessages(message, action = AntiRaidActions.WARN, mensajes, canalLogsId) {
    const user = message.author;
  
    // Verificar si el usuario ya ha enviado muchos mensajes en un corto período de tiempo
    if (usersMassMessage.has(user.id)) {
      const messageCount = usersMassMessage.get(user.id);
      usersMassMessage.set(user.id, messageCount + 1);
  
      if (messageCount > mensajes) {
        // El usuario ha enviado más de 10 mensajes en poco tiempo, tomar la acción adecuada
        performAntiRaidActionMessage(user, action, message, canalLogsId);
      }
    } else {
      // Registrar al usuario con 1 mensaje
      usersMassMessage.set(user.id, 1);
      setTimeout(() => {
        // Eliminar al usuario de la colección después de un tiempo para reiniciar el conteo
        usersMassMessage.delete(user.id);
      }, 60000); // Reiniciar el conteo después de 60 segundos
    }
  }
// Función para realizar la acción de anti-raid según la configuración de mensaje
async function performAntiRaidActionMessage(target, action, message, canalLogsId) {
  const guild = message.guild;


  switch (action) {
    case AntiRaidActions.WARN:
      try {
        const banBuilder = new EmbedBuilder()
          .setTitle('Usuario Avisado')
          .setDescription(`${target} ha sido advertido por actividad de raid.`);

        const canalLogs = guild.channels.cache.get(canalLogsId);
        if (canalLogs && canalLogs.isText()) {
          await canalLogs.send({ embeds: [banBuilder] });
          console.log(`Se envió un aviso al canal de logs (${canalLogsId}) por actividad de raid en ${target}.`);
        } else {
          console.error(`El canal de logs (${canalLogsId}) no es válido o no se encontró en el servidor.`);
        }
      } catch (error) {
        console.error('Error al enviar el aviso al canal de logs:', error);
      }
      break;
    case AntiRaidActions.MUTE:
      try {
        // Realizar acción de silenciamiento (ejemplo)
        muteUntil.setDate(muteUntil.getDate() + 1);
        const muteBuilder = new EmbedBuilder()
        .setTitle('Usuario muteado')
        .setDescription(`${target} ha sido muteado por actividad de raid o spam.`);

      const canalLogs = guild.channels.cache.get(canalLogsId);
      if (canalLogs) {
        await canalLogs.send({ embeds: [muteBuilder] });
        console.log(`Se envió un aviso al canal de logs (${canalLogsId}) por actividad de raid en ${target}.`);
      } else {
        console.error(`El canal de logs (${canalLogsId}) no es válido o no se encontró en el servidor.`);
      }
        await message.member.disableCommunicationUntil(muteUntil, `Muteado por actividad de raid o spam`).catch(err => {});
        
        console.log(`Silenciando a ${target}`);
      } catch (error) {
        console.error('Error al silenciar al usuario:', error);
      }
      break;
    case AntiRaidActions.DELETE:
      try {
        // Realizar acción de eliminar mensaje (ejemplo)
        message.delete().catch(err => {});;
        const deleteBuilder = new EmbedBuilder()
        .setTitle('Mensaje eliminado')
        .setDescription(`Se ha elimninado el mensaej de ${target} por actividad de raid o spam.`);
      const canalLogs = guild.channels.cache.get(canalLogsId);
      if (canalLogs) {
        await canalLogs.send({ embeds: [deleteBuilder] });
        console.log(`Se envió un aviso al canal de logs (${canalLogsId}) por actividad de raid en ${target}.`);
      } else {
        console.error(`El canal de logs (${canalLogsId}) no es válido o no se encontró en el servidor.`);
      }
      } catch (error) {
        console.error('Error al borrar el mensaje del usuario:', error);
      }
      break;
    case AntiRaidActions.KICK:
      try {
        // Realizar acción de expulsión (ejemplo)
        message.member.kick({ reason: "raid detectado" }).catch(err => {});;
        const kcikBuilder = new EmbedBuilder()
        .setTitle('Usuario expulsado')
        .setDescription(`${target} ha sido expulsado por actividad de raid.`);

      const canalLogs = guild.channels.cache.get(canalLogsId);
      if (canalLogs ) {
        await canalLogs.send({ embeds: [kcikBuilder] });
        console.log(`Se envió un aviso al canal de logs (${canalLogsId}) por actividad de raid en ${target}.`);
      } else {
        console.error(`El canal de logs (${canalLogsId}) no es válido o no se encontró en el servidor.`);
      }
        console.log(`Expulsando a ${target}`);
      } catch (error) {
        console.error('Error al expulsar al usuario:', error);
      }
      break;
    case AntiRaidActions.BAN:
      try {
        // Realizar acción de ban (ejemplo)
        console.log(`Baneando a ${target}`);
    
        message.member.ban({ reason: "Raid detectado" }).catch(err => {});;

        const banBuilder = new EmbedBuilder()
          .setTitle('Usuario Baneado')
          .setDescription(`El usuario ${target} ha sido baneado por spam.`);

        const canalLogs = guild.channels.cache.get(canalLogsId);
        if (canalLogs) {
          await canalLogs.send({ embeds: [banBuilder] });
          console.log(`Se envió un aviso de ban al canal de logs (${canalLogsId}) por actividad de raid en ${target}.`);
        } else {
          console.error(`El canal de logs (${canalLogsId}) no es válido o no se encontró en el servidor.`);
        }
      } catch (error) {
        console.error('Error al banear al usuario:', error);
      }
      break;
    case AntiRaidActions.NONE:
    default:
      // No realizar ninguna acción
      const canalLogs = guild.channels.cache.get(canalLogsId);
      if (canalLogs) {
        await canalLogs.send({embeds: [new EmbedBuilder().setTitle('Aviso').setDescription(`Se esta detectando actividad de spam o raid en el servidor ${target}`).setColor('Red').setTimestamp()]});
        console.log(`Se envió un aviso al canal de logs (${canalLogsId}) por actividad de raid en ${target}.`);
      } else {
        console.error(`El canal de logs (${canalLogsId}) no es válido o no se encontró en el servidor.`);
      }
      break;
  }
}

// Función para realizar la acción de anti-raid según la configuración de canal
async function performAntiRaidActionChannel(channel, target, action, canalLogsId) {
  const guild = channel.guild;

  switch (action) {
    case AntiRaidActions.WARN:
      try {
        const banBuilder = new EmbedBuilder()
          .setTitle('Usuario Avisado')
          .setDescription(`${target} ha sido advertido por actividad de raid en el canal ${channel.name}.`);

        const canalLogs = guild.channels.cache.get(canalLogsId);
        if (canalLogs && canalLogs.isText()) {
          await canalLogs.send({ embeds: [banBuilder] });
          console.log(`Se envió un aviso al canal de logs (${canalLogsId}) por actividad de raid en ${target}.`);
        } else {
          console.error(`El canal de logs (${canalLogsId}) no es válido o no se encontró en el servidor.`);
        }
      } catch (error) {
        console.error('Error al enviar el aviso al canal de logs:', error);
      }
      break;
    case AntiRaidActions.DELETE:
      try {
        // Realizar acción de eliminar canal (ejemplo)
        channel.delete().catch(err => {});;
      } catch (error) {
        console.error('Error al borrar el canal:', error);
      }
      break;
    case AntiRaidActions.KICK:
      try {
        // Realizar acción de expulsión (ejemplo)
        const user = channel.guild.members.cache.get(target);
        user.kick({ reason: "raid detectado" }).catch(err => {});;
        console.log(`Expulsando a ${target}`);
      } catch (error) {
        console.error('Error al expulsar al usuario:', error);
      }
      break;
    case AntiRaidActions.BAN:
      try {
        // Realizar acción de ban (ejemplo)
        console.log(`Baneando a ${target}`);
        const user = channel.guild.members.cache.get(target);
        user.ban({ reason: "Raid detectado" }).catch(err => {});;
      } catch (error) {
        console.error('Error al banear al usuario:', error);
      }
      break;
    case AntiRaidActions.NONE:
    default:
      // No realizar ninguna acción
      break;
  }
}

module.exports = {
  AntiRaidActions,
  handleMassMessages,
  performAntiRaidActionMessage,
  performAntiRaidActionChannel,
};
