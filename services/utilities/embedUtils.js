// embedUtils.js

const { MessageEmbed } = require('discord.js');
const logger = require('../../infrastructure/logging/logger');

async function sendEmbedMessage(channel, embed) {
    try {
        const embedMessage = new MessageEmbed(embed);
        await channel.send({ embeds: [embedMessage] });
        logger.info('embedUtils', `Mensaje embebido enviado a ${channel.name}`);
    } catch (error) {
        logger.error('embedUtils', 'Error al enviar mensaje embebido:', error);
        throw error;
    }
}

module.exports = {
    sendEmbedMessage
};
