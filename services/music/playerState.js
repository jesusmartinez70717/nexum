const { createAudioPlayer, AudioPlayerStatus, createAudioResource, VoiceConnectionStatus } = require('@discordjs/voice');

const playerState = {
    currentPlayer: null,
    voiceConnection: null,
    currentResource: null,
    playlist: [],
    loop: false, // Variable para controlar el modo de repetición

    createPlayer: function() {
        this.currentPlayer = createAudioPlayer();
        this.currentPlayer.on(AudioPlayerStatus.Idle, () => {
            if (this.loop && this.currentResource) {
                // Repetir la canción actual si el modo de repetición está activado
                this.currentPlayer.play(this.currentResource);
            } else {
                this.playNext();
            }
        });
        this.currentPlayer.on('error', error => {
            console.error('Error en el reproductor de audio:', error);
            this.handleError();
        });
    },

    handleError: function() {
        this.voiceConnection?.destroy();
        this.voiceConnection = null;
        this.currentPlayer = null;
        this.currentResource = null;
    },

    playNext: function() {
        if (this.playlist.length > 0) {
            const nextResource = this.playlist.shift();
            this.currentResource = nextResource;
            if (!this.currentPlayer) {
                this.createPlayer();
            }
            if (this.voiceConnection) {
                this.voiceConnection.subscribe(this.currentPlayer);
            }
            this.currentPlayer.play(nextResource);
        } else {
            this.voiceConnection?.destroy();
            this.voiceConnection = null;
            this.currentPlayer = null;
            this.currentResource = null; // Clear current resource when no songs are left
        }
    },

    setVolume: function(volume) {
        if (this.currentResource) {
            this.currentResource.volume.setVolume(volume);
        }
    },

    getPlaylist: function() {
        return this.playlist.map(resource => resource.metadata?.title || 'Canción desconocida');
    },

    removeFromPlaylist: function(indexOrName) {
        if (typeof indexOrName === 'number') {
            if (indexOrName >= 0 && indexOrName < this.playlist.length) {
                this.playlist.splice(indexOrName, 1);
                return true;
            }
        } else if (typeof indexOrName === 'string') {
            const index = this.playlist.findIndex(resource => {
                const title = resource.metadata?.title || 'Canción desconocida';
                return title.toLowerCase().includes(indexOrName.toLowerCase());
            });
            if (index !== -1) {
                this.playlist.splice(index, 1);
                return true;
            }
        }
        return false;
    },

    toggleLoop: function() {
        this.loop = !this.loop; // Alternar el modo de repetición
        return this.loop;
    }
};

module.exports = playerState;
