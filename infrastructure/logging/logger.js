const pc = require('picocolors');

const logger = {
  success(source, message) {
    console.log(`${pc.green(`[${source}]`)} ${pc.green(message)}`);
  },

  error(source, message) {
    console.log(`${pc.red(`[${source}]`)} ${pc.red(message)}`);
  },

  info(source, message) {
    console.log(`${pc.blue(`[${source}]`)} ${pc.blue(message)}`);
  },

  warn(source, message) {
    console.log(`${pc.yellow(`[${source}]`)} ${pc.yellow(message)}`);
  },

  debug(source, message) {
    console.log(`${pc.magenta(`[${source}]`)} ${pc.magenta(message)}`);
  }
};

module.exports = logger;