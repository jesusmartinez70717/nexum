const { Client } = require('discord.js');
const EventHandler = require('./handlers/EventHandler.js');
const { connectDB } = require('./db/mongoDb')
require('dotenv').config();
const client = new Client({ intents: [
      'GuildVoiceStates', 'AutoModerationConfiguration', 'AutoModerationExecution',
      'DirectMessageReactions', 'DirectMessageTyping', 'DirectMessages', 'GuildBans',
      'GuildEmojisAndStickers', 'GuildIntegrations', 'GuildInvites', 'GuildMembers',
      'GuildMessageReactions', 'GuildMessageTyping', 'GuildMessages', 'GuildModeration',
      'GuildPresences', 'GuildScheduledEvents', 'GuildVoiceStates', 'GuildWebhooks', 'Guilds',
      'MessageContent'
    ]});

  client.snipes = new Map();
EventHandler.loadEvents(client, './src/events')
connectDB()
client.login(process.env.TOKEN_BOT)