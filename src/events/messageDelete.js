const logger = require("../../infrastructure/logging/logger.js")
const snipedMessage = require('../../services/moderation/snipedMessage.js')
const DeleteMessageSystem = require('../../services/moderation/deleteMessageSystem.js')
module.exports = {
    name: 'messageDelete',
    execute(client, message) {
        snipedMessage(client, message)
        DeleteMessageSystem(client, message)
    }
}