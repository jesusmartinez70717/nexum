const reportSchema = require('../../db/models/reportModel.js')
const { EmbedBuilder } = require('discord.js')
module.exports = {
    name: 'interactionCreate',
   async execute(client, interaction) {
        if(!interaction.isModalSubmit()) return

        if(interaction.customId === 'modal') {
            const contact = interaction.fields.getTextInputValue('contact')
            const issue = interaction.fields.getTextInputValue('issue')
            const description = interaction.fields.getTextInputValue('description')

            const member = interaction.user.id;
            const tag = interaction.user.tag;
            const server = interaction.guild.name;

            const embed = new EmbedBuilder()
            .setColor('Red')
            .setTitle("Nuevo Reporte")
            .addFields(
                {name: 'Forma de contacto', value: `${contact}`},
                {name: 'Problema reportado', value: `${issue}`},
                {name: 'Descripción del problema', value: `${description}`}
            )

            const data = await reportSchema.findOne({Guild: interaction.guild.id})
            if(!data) return;
            if(data) {
                const ChannelId = data.Channel
                const channel = interaction.guild.channels.cache.get(ChannelId)

                channel.send({embeds: [embed]})
                await interaction.reply({content: 'Tu reporte fue enviado', ephemeral: true})
            }
        }
    }
}