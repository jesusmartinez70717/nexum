const AntiRaid = require('../../services/moderation/antiRaid.js');
const AntiRaidConfig = require('../../db/models/antiraidModel.js'); // Importa el modelo de configuración AntiRaid

module.exports = {
  name: 'messageCreate',
  async execute(client, message) {
    try {
          // Evitar que el bot responda a sus propios mensajes
      if (message.author.id === client.user.id) return;
      // Obtener la configuración AntiRaid específica para el servidor desde MongoDB
      const guildID = message.guild.id;
      const antiRaidConfig = await AntiRaidConfig.findOne({ guildId: guildID });

      if (!antiRaidConfig || message.author.me) {
        // Si no hay configuración AntiRaid para este servidor, no hacer nada
        return;
      }

      // Verificar si se debe detectar el envío masivo de mensajes
      if (antiRaidConfig.detectarMensajes) {
        AntiRaid.handleMassMessages(message, antiRaidConfig.accion, antiRaidConfig.mensajes, antiRaidConfig.canalLogsId);
      }

    } catch (err) {
        console.log(err);
    }
}}