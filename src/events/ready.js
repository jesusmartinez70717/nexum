const logger = require("../../infrastructure/logging/logger.js")
const { join } = require('node:path')
const SlashCommandHandler = require('../../handlers/SlashCommandHandler.js')
module.exports = {
    name: 'ready',
    execute(client) {
        const slashHandler = new SlashCommandHandler(client, join(__dirname, '../SlashCommand'))
        slashHandler.loadSlashCommands()
        slashHandler.listenForInteractions()

        logger.info('EventReady',` Ready! Logged in as ${client.user.tag}` )

        client.user.setActivity('Tu compañero de codigo abierto', {type: "STREAMING"})
    }
}