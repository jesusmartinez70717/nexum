const { ButtonBuilder, EmbedBuilder, ButtonStyle, ActionRowBuilder, ComponentType, ChannelType} = require('discord.js')
module.exports = {
    name: 'guildCreate',
   async execute(client, guild) {
       if(!guild) return;
       const sendChannel = await client.channels.fetch('1254626626117959712');

       const name = guild.name;
       const id = guild.id;
       const owner = await guild.members.fetch(guild.ownerId)
       const memberCount = await guild.members.cache.size;
       const botCount = (await guild.members.fetch()).filter(member => member.user.bot).size
       const clientGuildCount = await client.guilds.cache.size
       const joinTime = `<t:${Math.floor(Date.now() / 1000)}:R>`

       const embed = new EmbedBuilder()
       .setColor('Red')
       .setTitle(`Union a un nuevo servidor`)
       .addFields(
        {name: 'Nombe del servidor', value: `\`${name}\``},
        {name: 'Id del servidor', value: `\`${id}\``},
        {name: 'Dueño del servidor', value: `\`${owner.user.username} (${owner.id})\``},
        {name: 'Miembros en el servidor', value: `\`${memberCount}\``},
        {name: 'bots en el servidor', value: `\`${botCount}\``},
        {name: 'Fecha de union al servidor', value: `${joinTime}`}
       )
       .setDescription(`Ahora estoy en ${clientGuildCount} servidores. Usa el boton a continuacion para enviarte una invitacion a este servidor`)
       .setFooter({text: 'Log de union a servidor'})
       .setTimestamp()

       const button = new ButtonBuilder()
       .setCustomId('fetchInviteforJoin')
       .setLabel('Enviar invitacion')
       .setStyle(ButtonStyle.Danger)

       const row = new ActionRowBuilder().addComponents(button)

       const msg = await sendChannel.send({embeds: [embed], components: [row]}).catch(err => {})

       let time = 300000;
       const collector = await msg.createMessageComponentCollector({
        componentType: ComponentType.Button,
        time
       });

       collector.on('collect', async i => {
        if(i.customId == 'fetchInviteforJoin') {
            let channel;
            const channels = await guild.channels.cache.filter(c => c.type === ChannelType.GuildText)

            for(const c of channels.values()) {
                channel = c;
                break;
            }
            if(!channel) return await i.reply({content: `No se encontro ningun canal para enviar la invitacion`, ephemeral: true})
            const invite = await channel.createInvite().catch(err => {})
        await i.reply({content: `Aqui esta la invitacion al servidor ${name}: https://discord.gg/${invite.code}`, ephemeral: true})
        }
       })

       collector.on('end', async () => {
        button.setDisabled(true);
        embed.setFooter({text: `Log de union a servidor - Expiro el tiempo para invitacion`})
        await msg.edit({embeds: [embed], components: [row]})
       })
    }
}