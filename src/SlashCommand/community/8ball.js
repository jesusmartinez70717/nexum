const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('8ball')
        .setDescription('Para jugar la bola mágica')
        .addStringOption(option => 
            option
                .setName('pregunta')
                .setDescription('¿Cuál es la pregunta que le quieres hacer a la bola mágica?')
                .setRequired(true)
        ),

    async execute(client, interaction) {
        const { options } = interaction;
        const question = options.getString('pregunta');

        // Lista de respuestas posibles
        const answers = [
            "Sí.",
            "No.",
            "Tal vez.",
            "Definitivamente.",
            "No cuentes con ello.",
            "Probablemente.",
            "Es seguro.",
            "Las perspectivas no son buenas.",
            "Sí, definitivamente.",
            "Mis fuentes dicen que no.",
            "Lo dudo mucho.",
            "Concentrate y pregunta otra vez.",
            "Sin duda."
        ];

        // Elegir una respuesta aleatoria
        const choice = answers[Math.floor(Math.random() * answers.length)];

        // Crear un embed para la respuesta
        const embed = new EmbedBuilder()
            .setColor('#00FF00')
            .setTitle('🎱 Bola Mágica 🎱')
            .setDescription(`**Pregunta:** ${question}\n**Respuesta:** ${choice}`)
            .setTimestamp();

        await interaction.reply({ embeds: [embed] });
    }
};
