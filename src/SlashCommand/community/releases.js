const { SlashCommandBuilder, EmbedBuilder} = require('discord.js')
const notes = require('../../../db/models/releaseModel.js')
require('dotenv').config()
module.exports = {
    data: new SlashCommandBuilder()
    .setName('release-notes')
    .setDescription('Ver las notas de la version')
    .addSubcommand(command => command.setName('publicar').setDescription('Puedes publicar nuevas notas de version (Solo el creador)').addStringOption(option => option.setName('nuevas-notas').setDescription('Las nuevas notas a publicar').setRequired(true)))
    .addSubcommand(command => command.setName('ver').setDescription('Ver las notas de la version reciente')),
    async execute(client, interaction) {
        const { options } = interaction;
        const sub = options.getSubcommand()
        let data = await notes.find()

        async function sendMessage(message) {
            const embed = new EmbedBuilder()
            .setColor('Red')
            .setDescription(message)

            await interaction.reply({ embeds: [embed] })
        }

        async function updateNotes(update, version) {
            await notes.create({
                Updates: update,
                Date: Date.now(),
                Developer: interaction.user.username,
                Version: version
            });

            await sendMessage('Las nuevas notas de la version han sido subidas')
        }

        switch (sub) {
            case 'publicar':
                if(interaction.user.id !== process.env.BOT_OWNER_ID) return await sendMessage('Lo siento pero solo el creador del bot puede publicar nuevas notas')
                const update = options.getString('nuevas-notas')
            if(data.length > 0) {
                await notes.deleteMany();

                let version = 0;
                await data.forEach(async value => {
                    version += value.Version
                })
                await updateNotes(update, version + 0.1)
            } else {
                await updateNotes(update, 1.0)
            }
                break;
                case 'ver':
                if(data.length == 0) return await sendMessage('No hay notas publicadas actualmente')
                let string = ``;
            await data.forEach(async value => {
                string += `\`${value.Version}\` \n\n**Informacion de actualizacion:**\n\`\`\`${value.Updates}\`\`\`\n\n**Desarrollador de la actualizacion:** ${value.Developer}\n**Fecha de actualizacion:** <t:${Math.floor(value.Data / 1000)}:R>`
            })
            await sendMessage(`Notas de la version: ${string}`)
                break;
        }
    }
}