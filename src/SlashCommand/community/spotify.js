const { SlashCommandBuilder, EmbedBuilder, AttachmentBuilder } = require('discord.js');
const canvacord = require('canvacord');

module.exports = {
  data: new SlashCommandBuilder()
    .setName('spotify')
    .setDescription('Te muestra el status de un usuario en Spotify')
    .addUserOption(option => 
      option.setName('usuario')
        .setDescription('El usuario del que quieres ver el status')
        .setRequired(true)
    ),

  async execute(client, interaction) {
    try {
      const user = await fetchUser(interaction);
      if (!user) return;

      const spotifyStatus = getSpotifyStatus(user);
      if (!spotifyStatus) {
        return await interaction.reply({ content: `${user.user.username} no está escuchando nada en Spotify`, ephemeral: true });
      }

      const spotifyCard = await createSpotifyCard(spotifyStatus);
      await sendSpotifyEmbed(interaction, user, spotifyCard);

    } catch (error) {
      console.error(error);
      await interaction.reply({ content: 'Ocurrió un error al intentar obtener el estado de Spotify.', ephemeral: true });
    }
  }
};

async function fetchUser(interaction) {
  const us = interaction.options.getUser('usuario');
  const user = await interaction.guild.members.fetch(us.id);

  if (user.bot) {
    await interaction.reply({ content: 'No se puede ver el estado en Spotify de un bot', ephemeral: true });
    return null;
  }

  return user;
}

function getSpotifyStatus(user) {
  const activity = user.presence?.activities?.find(act => act.name === 'Spotify' && act.type === 2); // 'LISTENING' type is 2
  return activity || null;
}

async function createSpotifyCard(status) {
  const image = `https://i.scdn.co/image/${status.assets.largeImage.slice(8)}`;
  const { details: name, state: artist, assets: { largeText: album }, timestamps: { start, end } } = status;

  const card = new canvacord.Spotify()
    .setAuthor(artist)
    .setAlbum(album)
    .setStartTimestamp(start)
    .setEndTimestamp(end)
    .setImage(image)
    .setTitle(name);

  return await card.build();
}

async function sendSpotifyEmbed(interaction, user, spotifyCard) {
  const attachment = new AttachmentBuilder(spotifyCard, { name: 'spotify.png' });

  const embed = new EmbedBuilder()
    .setTitle('Spotify')
    .setAuthor({ name: user.user.username, iconURL: user.user.displayAvatarURL() })
    .setImage('attachment://spotify.png')
    .setTimestamp();

  await interaction.reply({ embeds: [embed], files: [attachment] });
}
