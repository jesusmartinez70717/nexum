const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('membercount')
        .setDescription('Muestra información sobre los miembros del servidor.'),
    async execute(client, interaction) {
        // Obtener la cantidad de miembros del servidor
        const memberCount = interaction.guild.memberCount;

        // Obtener la cantidad de miembros online
        const onlineCount = interaction.guild.members.cache.filter(member => member.presence.status !== 'offline').size;

        // Obtener la cantidad de bots
        const botCount = interaction.guild.members.cache.filter(member => member.user.bot).size;

        const embed = new EmbedBuilder()
            .setColor('#0099ff')
            .setTitle('Información de miembros del servidor')
            .addFields(
                { name: 'Usuarios totales', value: memberCount.toString(), inline: true },
                { name: 'Usuarios online', value: onlineCount.toString(), inline: true },
                { name: 'Cantidad de bots', value: botCount.toString(), inline: true },
            )
            .setTimestamp();

        // Responder con el embed
        await interaction.reply({ embeds: [embed] });
    },
};
