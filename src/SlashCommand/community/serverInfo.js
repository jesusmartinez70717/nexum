const { SlashCommandBuilder, EmbedBuilder} = require('discord.js')

module.exports = {
    data: new SlashCommandBuilder()
    .setName('serverinfo')
    .setDescription('Te da informacion del servidor'),
    async execute(client, interaction) {

        const { guild } = interaction;
        const { members, name, ownerId, createdTimestamp, memberCount } = guild;
        const icon = guild.iconURL() || 'https://imgs.search.brave.com/hOIK1nbzrpfttuel3Px604RyTQ88Cn6cErDU15JNgB8/rs:fit:500:0:0/g:ce/aHR0cHM6Ly9zdGF0/aWMtMDAuaWNvbmR1/Y2suY29tL2Fzc2V0/cy4wMC9kaXNjb3Jk/LWljb24tNTEyeDUx/Mi1mcmJzMTRyci5w/bmc'
        const roles = guild.roles.cache.size
        const emojis = guild.emojis.cache.size
        const id = guild.id

        let baseVerification = guild.verificationLevel
        if(baseVerification == 0) baseVerification = 'Nada'
        if(baseVerification == 1) baseVerification = 'Baja'
        if(baseVerification == 2) baseVerification = 'Media'
        if(baseVerification == 3) baseVerification = 'Alta'
        if(baseVerification == 4) baseVerification = 'Muy alta'

        const embed = new EmbedBuilder()
        .setColor('Red')
        .setThumbnail(icon)
        .setAuthor({name: name, iconURL: icon})
        .setFooter({text: `Server ID: ${id}`})
        .setTimestamp()
        .addFields(
            {name: 'Nombre', value: `${name}`, inline: false},
            {name: 'Fecha creacion', value: `<t:${parseInt(createdTimestamp / 1000)}:R>`, inline: true},
            {name: 'Dueño del servidor', value: `<@${ownerId}>`, inline: true},
            {name: 'Miembros del servidor', value: `${memberCount}`, inline: true},
            {name: 'Numero de roles', value: `${roles}`, inline: true},
            {name: 'Numero de emojis', value: `${emojis}`, inline: true},
            {name: 'Nivel de verificacion', value: `${baseVerification}`, inline: true},
            {name: 'Boost del servidor', value: `${guild.premiumSubscriptionCount}`, inline: true}
        )
        await interaction.reply({embeds: [embed]})
    }
}