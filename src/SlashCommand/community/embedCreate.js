const { SlashCommandBuilder, EmbedBuilder} = require('discord.js')

module.exports = {
    data: new SlashCommandBuilder()
    .setName('embed-create')
    .setDescription('Crea un mensaje embed')
    .addStringOption(option => option.setName('titulo').setDescription('El titulo que tendra el mensaje').setRequired(true))
    .addStringOption(option => option.setName('descripcion').setRequired(true).setDescription('La descripcion que tendra el mensaje'))
    .addStringOption(option => option.setName('color').setDescription('El color del mensaje embed').setRequired(false).setMaxLength(6))
    .addStringOption(option => option.setName('imagen').setDescription('La imagen que deberia tener el embed').setRequired(false))
    .addStringOption(option => option.setName('miniatura').setDescription('La miniatura que deberia tener el embed').setRequired(false))
    .addStringOption(option => option.setName('field-name').setDescription('El nombre del field que deberia tener el embed').setRequired(false))
    .addStringOption(option => option.setName('field-value').setDescription('El valor del field del embed').setRequired(false))
    .addStringOption(option => option.setName('footer').setDescription('El pie de mensaje').setRequired(false)),
    async execute(client, interaction) {
        const { options } = interaction;
        const title = options.getString('titulo');
        const description = options.getString('descripcion');
        const color = options.getString('color') || '800000';
        const image = options.getString('imagen');
        const thumbnail = options.getString('miniatura');
        const fieldName = options.getString('field-name') || ' ';
        const fieldValue = options.getString('field-value') || ' ';
        const footer = options.getString('footer') || ' ';

        if(image) if(!image.startsWith('http')) return await interaction.reply({content: 'Esa no es una url valida de imagen', ephemeral: true})
        if(thumbnail) if(!thumbnail.startsWith('http')) return await interaction.reply({content: 'Esa no es una url valida de miniatura', ephemeral: true})

        const embed = new EmbedBuilder()
        .setTitle(title)
        .setDescription(description)
        .setColor(`#${color}`)
        .setImage(image)
        .setThumbnail(thumbnail)
        .setTimestamp()
        .addFields({name: `${fieldName}`, value: `${fieldValue}`})
        .setFooter({text: `${footer}`, iconURL: interaction.member.displayAvatarURL({ dynamic: true})})

        await interaction.reply({content: 'El mensaje embed se va a enviar ya', ephemeral: true})
        await interaction.channel.send({embeds: [embed]})
    }

}