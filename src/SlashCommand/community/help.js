const { SlashCommandBuilder, EmbedBuilder, ActionRowBuilder, ButtonBuilder, ButtonStyle} = require('discord.js');

module.exports = {
    data: new SlashCommandBuilder()
    .setName('help')
    .setDescription('Te da ayuda con los comandos'),

    async execute(client, interaction) {
        const embed = new EmbedBuilder()
        .setColor('Blue')
        .setTitle('Centro de ayuda')
        .setDescription('Guia de comandos:')
        .addFields({name: "pagina 1", value: 'Ayuda y recursos (boton 1)'})
        .addFields({name: "pagina 2", value: 'Comandos de la comunidad (boton 2)'})
        .addFields({name: "pagina 3", value: 'Comandos de moderacion (boton 3)'})
        .addFields({name: 'Pagina 4', value: 'Mas informacion sobre el bot (Boton 4)'})

        const EmbedCommunityCommand = new EmbedBuilder()
        .setTitle('Comandos de la comunidad')
        .setColor('Blue')
        .addFields({name: '/help', value: 'Te da una lista de ayuda e informacion'})
        .addFields({name: '/membercount', value: 'Te dice los miembros totales en el servidor'})
        .setTimestamp()

        const EmbedModerationCommand = new EmbedBuilder()
        .setTitle('Comandos de moderacion')
        .setColor('Blue')
        .addFields({name: '/warn', value: 'Le da una advertencia a un miembro del servidor'})
        .addFields({name: '/mute', value: 'Aisla a un usuario del servidor'})
        .setTimestamp()

        const InfoExtra = new EmbedBuilder()
        .setColor('#0099ff')
        .setTitle('Información de NexuM Bot')
        .setThumbnail(client.user.displayAvatarURL())
        .setDescription('NexuM es un bot de Discord de código abierto, diseñado para ofrecer una experiencia completa y personalizable para cualquier servidor.')
        .addFields(
            { name: 'Características Principales', value: '• Moderación avanzada\n• Entretenimiento\n• Utilidades\n• Personalización\n• Y mucho más!' },
            { name: 'Creador', value: '• Nombre: Jesús Martínez\n• GitLab: [Repositorio NexuM](https://gitlab.com/jesusmartinez70717/nexum)' },
            { name: 'Enlaces Útiles', value: '[Repositorio GitLab](https://gitlab.com/jesusmartinez70717/nexum)\n[Soporte](https://discord.gg/r3nXHmyAzE)' },
            { name: 'Versión', value: '1.0.0', inline: true },
            { name: 'Librería', value: 'Discord.js v14', inline: true },
            { name: 'Prefijo', value: '/', inline: true },
            { name: 'Fecha de Creación', value: 'Junio 2024' }
        )
        .setFooter({ text: 'Gracias por usar NexuM Bot!', iconURL: client.user.displayAvatarURL() })
        .setTimestamp();

        const buttons = new ActionRowBuilder()
        .addComponents(
            new ButtonBuilder()
            .setCustomId('page1')
            .setLabel('Pagina 1')
            .setStyle(ButtonStyle.Success),

            new ButtonBuilder()
            .setCustomId('page2')
            .setLabel('Pagina 2')
            .setStyle(ButtonStyle.Success),

            new ButtonBuilder()
            .setCustomId('page3')
            .setLabel('Pagina 3')
            .setStyle(ButtonStyle.Success),

            new ButtonBuilder()
            .setCustomId('page4')
            .setLabel('Pagina 4')
            .setStyle(ButtonStyle.Success),
        );
        const message = await interaction.reply({embeds: [embed], components: [buttons], ephemeral: true});
        const collector = await message.createMessageComponentCollector();

        collector.on('collect', async i => {
            if (i.customId === 'page1') {
                await i.update({ embeds: [embed], components: [buttons], ephemeral: true});
            }
            if (i.customId === 'page2') {
                await i.update({ embeds: [EmbedCommunityCommand], components: [buttons], ephemeral: true});
            }
            if (i.customId === 'page3') {
                await i.update({ embeds: [EmbedModerationCommand], components: [buttons], ephemeral: true});
            }
            if (i.customId === 'page4') {
                await i.update({ embeds: [InfoExtra], components: [buttons], ephemeral: true});
            }
          });
    }
}