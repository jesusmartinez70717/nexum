const { SlashCommandBuilder, EmbedBuilder, PermissionsBitField, ChannelType, ModalBuilder, TextInputBuilder, TextInputStyle, ActionRowBuilder} = require('discord.js')
const reportSchema = require('../../../db/models/reportModel.js')

module.exports = {
    data: new SlashCommandBuilder()
    .setName('report')
    .setDescription('Es el comando para reportar'),

    async execute(client, interaction) {
        const data = await reportSchema.findOne({Guild: interaction.guild.id})
        if(!data) return await interaction.reply({content: 'El sistema de reportes no esta configurado en este servidor', ephemeral: true})
        if(data) {
            const modal = new ModalBuilder()
            .setTitle('Formulario de reporte')
            .setCustomId('modal')

            const contact = new TextInputBuilder()
            .setCustomId('contact')
            .setRequired(true)
            .setLabel('Brindanos una forma de contacto')
            .setPlaceholder('El discord suele ser mejor')
            .setStyle(TextInputStyle.Short)

            const issue = new TextInputBuilder()
            .setCustomId('issue')
            .setRequired(true)
            .setLabel('¿Que te gustaria reportar?')
            .setPlaceholder('¿Un miembro, un problema con el servidor o algo mas?')
            .setStyle(TextInputStyle.Short)

            const description = new TextInputBuilder()
            .setCustomId('description')
            .setRequired(true)
            .setLabel('Describe el problema que estas reportando')
            .setPlaceholder('Se lo mas detallado posible')
            .setStyle(TextInputStyle.Paragraph)

            const firstActionRow = new ActionRowBuilder().addComponents(contact)
            const secondtActionRow = new ActionRowBuilder().addComponents(issue)
            const thirdtActionRow = new ActionRowBuilder().addComponents(description)

            modal.addComponents(firstActionRow, secondtActionRow, thirdtActionRow)
            interaction.showModal(modal)
        }
    }
}