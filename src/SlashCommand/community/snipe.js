const { SlashCommandBuilder, EmbedBuilder} = require('discord.js');

module.exports = {
    data: new SlashCommandBuilder()
    .setName('snipe')
    .setDescription('Te devuelve el ultimo mensaje borrado'),
    async execute(client, interaction) {

        const msg = client.snipes.get(interaction.channel.id)
        if(!msg) return interaction.reply({content: 'No hay ningun mensaje eliminado actualmente', ephemeral: true})
        
        const ID = msg.author.id
        const member = interaction.guild.members.cache.get(ID)
        const url = member.displayAvatarURL()

        const embed = new EmbedBuilder()
        .setColor('Red')
        .setTitle(`Mensaje borrado de ${member.user.tag}`)
        .setDescription(`${msg.content}`)
        .setTimestamp()
        .setFooter({text: `Id del miembro: ${ID}`, iconURL: url})

        if(msg.image) embed.setImage(msg.image)

        await interaction.reply({embeds: [embed]})
    }
}