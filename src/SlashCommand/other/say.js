const { SlashCommandBuilder, PermissionFlagsBits, EmbedBuilder} = require('discord.js');

module.exports = {
    data: new SlashCommandBuilder()
    .setName('say')
    .setDescription('El bot envia el mensaje proporcionado por ti al canal especifico')
    .addChannelOption(option => option.setName('canal').setDescription('El canal en el que se enviara el mensaje').setRequired(true))
    .addStringOption(option => option.setName('mensaje').setDescription('El mensaje que enviara el bot').setRequired(true))
    .setDefaultMemberPermissions(PermissionFlagsBits.SendMessages),

    async execute(client, interaction) {
        const channel = interaction.options.getChannel('canal');
        const message = interaction.options.getString('mensaje');

        channel.send(message)
        interaction.reply({content: 'Tu mensaje fue enviado', ephemeral: true})
    }
}