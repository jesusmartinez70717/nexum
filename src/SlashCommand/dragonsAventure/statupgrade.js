const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');
const profileSchema = require('../../schema/economy/profileSchema.js');
const dragonSchema = require('../../schema/miniGames/dragons.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('mejorarestadistica')
        .setDescription('Mejora una estadística de tu dragón')
        .addStringOption(option =>
            option.setName('id')
                .setDescription('ID del dragón')
                .setRequired(true)
        )
        .addStringOption(option =>
            option.setName('stat')
                .setDescription('Estadística a mejorar (ataque, defensa, velocidad, hp)')
                .setRequired(true)
                .addChoices('Ataque', 'attack')
                .addChoices('Defensa', 'defense')
                .addChoices('Velocidad', 'speed')
                .addChoices('HP', 'hp')
        ),

        async execute(client, interaction, profileData) {
        const dragonId = interaction.options.getString('id');
        const stat = interaction.options.getString('stat');
        const userId = interaction.user.id;

        const dragon = await dragonSchema.findOne({ id: dragonId, owner: profileData.userId });
        if (!dragon) {
            return interaction.reply('No se encontró el dragón o no te pertenece.');
        }

        if (profileData.balance < 50) {
            return interaction.reply('No tienes suficientes Nexis para mejorar la estadística de tu dragón.');
        }

        switch (stat) {
            case 'attack':
                dragon.attack++;
                break;
            case 'defense':
                dragon.defense++;
                break;
            case 'speed':
                dragon.speed++;
                break;
            case 'hp':
                dragon.hp += 10;
                break;
            default:
                return interaction.reply('Estadística no válida.');
        }

        profileData.balance -= 50;

        await dragon.save();
        await profileData.save();

        const embed = new EmbedBuilder()
            .setTitle(`¡${dragon.name} ha mejorado su ${stat}!`)
            .setDescription(`Nueva estadística:\n${stat.charAt(0).toUpperCase() + stat.slice(1)}: ${dragon[stat]}`)
            .setColor(0x00FF00);

        await interaction.reply({ embeds: [embed] });
    }
};
