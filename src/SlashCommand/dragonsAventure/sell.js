const { SlashCommandBuilder } = require('discord.js');
const profileSchema = require('../../schema/economy/profileSchema.js');
const dragonSchema = require('../../schema/miniGames/dragons.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('vender')
        .setDescription('Vende uno de tus dragones')
        .addStringOption(option =>
            option.setName('id')
                .setDescription('ID del dragón que quieres vender')
                .setRequired(true)
        )
        .addNumberOption(option =>
            option.setName('precio')
                .setDescription('Precio al que deseas vender el dragón')
                .setRequired(true)
        ),

        async execute(client, interaction, profileData) {
        const dragonId = interaction.options.getString('id');
        const price = interaction.options.getNumber('precio');
        const userId = interaction.user.id;

        const dragon = await dragonSchema.findOne({ id: dragonId, owner: profileData.userId });
        if (!dragon) {
            return interaction.reply('No se encontró el dragón o no te pertenece.');
        }

        const recommendedPrice = (dragon.attack + dragon.defense + dragon.speed + dragon.hp) * 0.5;

        await interaction.reply({
            content: `El precio recomendado para tu ${dragon.name} es ${recommendedPrice} Nexis. ¿Deseas usar este precio o establecer otro?`,
            components: [
                {
                    type: 1,
                    components: [
                        {
                            type: 2,
                            label: 'Usar precio recomendado',
                            style: 1,
                            custom_id: 'use_recommended'
                        },
                        {
                            type: 2,
                            label: 'Establecer otro precio',
                            style: 2,
                            custom_id: 'set_price'
                        }
                    ]
                }
            ],
            ephemeral: true
        });

        const filter = i => i.user.id === userId;
        const collector = interaction.channel.createMessageComponentCollector({ filter, time: 15000 });

        collector.on('collect', async i => {
            if (i.customId === 'use_recommended') {
                dragon.isForSale = true;
                dragon.price = recommendedPrice;
                await dragon.save();

                await i.update({ content: `¡Has puesto a la venta tu ${dragon.name} por ${recommendedPrice} Nexis!`, components: [] });
            } else if (i.customId === 'set_price') {
                dragon.isForSale = true;
                dragon.price = price;
                await dragon.save();

                await i.update({ content: `¡Has puesto a la venta tu ${dragon.name} por ${price} Nexis!`, components: [] });
            }
        });

        collector.on('end', collected => {
            if (collected.size === 0) {
                interaction.editReply({ content: 'Venta cancelada.', components: [] });
            }
        });
    }
};
