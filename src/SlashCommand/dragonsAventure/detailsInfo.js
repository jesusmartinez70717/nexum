const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');
const profileSchema = require('../../schema/economy/profileSchema.js');
const dragonSchema = require('../../schema/miniGames/dragons.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('dragondetalle')
        .setDescription('Muestra detalles de un dragón')
        .addStringOption(option =>
            option.setName('id')
                .setDescription('ID del dragón del que quieres ver detalles')
                .setRequired(true)
        ),

        async execute(client, interaction, profileData) {
        const dragonId = interaction.options.getString('id');
        const userId = interaction.user.id;

        const dragon = await dragonSchema.findOne({ id: dragonId, owner: profileData.userId });
        if (!dragon) {
            return interaction.reply('No se encontró el dragón o no te pertenece.');
        }

        const embed = new EmbedBuilder()
            .setTitle(`${dragon.name} (${dragon.uniqueName})`)
            .setDescription(`Nivel: ${dragon.level}\nAtaque: ${dragon.attack}\nDefensa: ${dragon.defense}\nVelocidad: ${dragon.speed}\nHP: ${dragon.hp}\nHabilidades: ${dragon.abilities.join(', ')}\nRareza: ${dragon.rarity}`)
            .setColor(0x00FF00);

        await interaction.reply({ embeds: [embed] });
    }
};
