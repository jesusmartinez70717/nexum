const { SlashCommandBuilder, ButtonBuilder, ButtonStyle, ActionRowBuilder } = require('discord.js');
const profileSchema = require('../../schema/economy/profileSchema.js');
const dragonSchema = require('../../schema/miniGames/dragons.js');
const initialDragons = require('../../../dragons.json');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('comprar')
        .setDescription('Compra un dragón')
        .addStringOption(option =>
            option.setName('tipo')
                .setDescription('Elige el tipo de dragón para comprar')
                .setRequired(true)
                .addChoices(
                    initialDragons.map(dragon => ({ name: dragon.name, value: dragon.type }))
                )
        )
        .addStringOption(option =>
            option.setName('id')
                .setDescription('ID del dragón que deseas comprar (opcional)')
                .setRequired(false)
        ),
    async execute(client, interaction, profileData) {
        const tipo = interaction.options.getString('tipo');
        const dragonId = interaction.options.getString('id');
        const userId = interaction.user.id;


        if (dragonId) {
            const dragonForSale = await dragonSchema.findOne({ id: dragonId, isForSale: true });
            if (!dragonForSale) {
                return interaction.reply({ content: 'El dragón que deseas comprar no está disponible.', ephemeral: true });
            }

            if (profileData.balance < dragonForSale.price) {
                return interaction.reply({ content: 'No tienes suficiente saldo para comprar este dragón.', ephemeral: true });
            }

            const confirmEmbed = {
                title: `Confirmar compra: ${dragonForSale.name}`,
                description: `¿Estás seguro de que quieres comprar este dragón por ${dragonForSale.price} Nexis?`,
                color: 'Red',
                fields: [
                    { name: 'Nombre', value: dragonForSale.uniqueName, inline: true },
                    { name: 'Ataque', value: dragonForSale.attack.toString(), inline: true },
                    { name: 'Defensa', value: dragonForSale.defense.toString(), inline: true },
                    { name: 'Velocidad', value: dragonForSale.speed.toString(), inline: true },
                    { name: 'HP', value: dragonForSale.hp.toString(), inline: true },
                    { name: 'Habilidades', value: dragonForSale.abilities.join(', '), inline: true },
                    { name: 'Rareza', value: dragonForSale.rarity, inline: true }
                ]
            };

            const row = new ActionRowBuilder()
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('confirm')
                        .setLabel('Confirmar')
                        .setStyle(ButtonStyle.SUCCESS),
                    new ButtonBuilder()
                        .setCustomId('cancel')
                        .setLabel('Cancelar')
                        .setStyle(ButtonStyle.DANGER)
                );

            await interaction.reply({ embeds: [confirmEmbed], components: [row], ephemeral: true });

            const filter = i => i.user.id === userId;
            const collector = interaction.channel.createMessageComponentCollector({ filter, time: 60000 });

            collector.on('collect', async i => {
                if (i.customId === 'confirm') {
                    profileData.balance -= dragonForSale.price;
                    profileData.inventory.dragons.push(dragonForSale._id);

                    dragonForSale.owner = profileData._id;
                    dragonForSale.isForSale = false;
                    dragonForSale.price = 0;

                    await profileData.save();
                    await dragonForSale.save();

                    const sellerProfile = await profileSchema.findById(dragonForSale.owner);
                    if (sellerProfile) {
                        sellerProfile.balance += dragonForSale.price;
                        await sellerProfile.save();
                        const sellerUser = await client.users.fetch(sellerProfile.userId);
                        if (sellerUser) {
                            await sellerUser.send(`Tu ${dragonForSale.name} ha sido comprado por ${dragonForSale.price} Nexis.`);
                        }
                    }

                    await i.update({ content: `¡Has comprado un ${dragonForSale.name} por ${dragonForSale.price} Nexis!`, embeds: [], components: [] });
                } else if (i.customId === 'cancel') {
                    await i.update({ content: 'Compra cancelada.', embeds: [], components: [] });
                }
            });

            collector.on('end', collected => {
                if (collected.size === 0) {
                    interaction.editReply({ content: 'Compra cancelada por tiempo.', embeds: [], components: [] });
                }
            });

        } else {
            const dragonData = initialDragons.find(dragon => dragon.type === tipo);
            if (!dragonData) {
                return interaction.reply({ content: 'El tipo de dragón seleccionado no está disponible.', ephemeral: true });
            }

            if (profileData.balance < dragonData.price) {
                return interaction.reply({ content: 'No tienes suficiente saldo para comprar este dragón.', ephemeral: true });
            }

            const confirmEmbed = {
                title: `Confirmar compra: ${dragonData.name}`,
                description: `¿Estás seguro de que quieres comprar este dragón por ${dragonData.price} Nexis?`,
                color: 0x00FF00,
                fields: [
                    { name: 'Nombre', value: dragonData.uniqueName, inline: true },
                    { name: 'Ataque', value: dragonData.attack.toString(), inline: true },
                    { name: 'Defensa', value: dragonData.defense.toString(), inline: true },
                    { name: 'Velocidad', value: dragonData.speed.toString(), inline: true },
                    { name: 'HP', value: dragonData.hp.toString(), inline: true },
                    { name: 'Habilidades', value: dragonData.abilities.join(', '), inline: true },
                    { name: 'Rareza', value: dragonData.rarity, inline: true }
                ]
            };

            const row = new ActionRowBuilder()
                .addComponents(
                    new ButtonBuilder()
                        .setCustomId('confirm')
                        .setLabel('Confirmar')
                        .setStyle(ButtonStyle.Success),
                    new ButtonBuilder()
                        .setCustomId('cancel')
                        .setLabel('Cancelar')
                        .setStyle(ButtonStyle.Danger)
                );

            await interaction.reply({ embeds: [confirmEmbed], components: [row], ephemeral: true });

            const filter = i => i.user.id === userId;
            const collector = interaction.channel.createMessageComponentCollector({ filter, time: 60000 });

            collector.on('collect', async i => {
                if (i.customId === 'confirm') {
                    const newDragon = new dragonSchema({
                        name: dragonData.name,
                        type: dragonData.type,
                        level: 1,
                        owner: profileData._id,
                        isForSale: false,
                        price: 0,
                        attack: dragonData.attack,
                        defense: dragonData.defense,
                        speed: dragonData.speed,
                        hp: dragonData.hp,
                        uniqueName: dragonData.uniqueName,
                        abilities: dragonData.abilities,
                        rarity: dragonData.rarity
                    });

                    await newDragon.save();

                    profileData.balance -= dragonData.price;
                    profileData.inventory.dragons.push(newDragon._id);
                    await profileData.save();

                    await i.update({ content: `¡Has comprado un ${dragonData.name} por ${dragonData.price} Nexis!`, embeds: [], components: [] });
                } else if (i.customId === 'cancel') {
                    await i.update({ content: 'Compra cancelada.', embeds: [], components: [] });
                }
            });

            collector.on('end', collected => {
                if (collected.size === 0) {
                    interaction.editReply({ content: 'Compra cancelada por tiempo.', embeds: [], components: [] });
                }
            });
        }
    }
};
