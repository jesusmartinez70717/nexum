const { SlashCommandBuilder } = require('discord.js');
const profileSchema = require('../../schema/economy/profileSchema.js');
const dragonSchema = require('../../schema/miniGames/dragons.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('liberar')
        .setDescription('Liberar un dragón de tu inventario')
        .addStringOption(option =>
            option.setName('id')
                .setDescription('ID del dragón que deseas liberar')
                .setRequired(true)
        ),

        async execute(client, interaction, profileData) {
        const dragonId = interaction.options.getString('id');
        const userId = interaction.user.id;


        const dragon = await dragonSchema.findOne({ id: dragonId, owner: profileData.userId });
        if (!dragon) {
            return interaction.reply('No se encontró el dragón o no te pertenece.');
        }

        await dragon.delete();

        await interaction.reply(`¡Has liberado a ${dragon.name} de tu inventario!`);
    }
};
