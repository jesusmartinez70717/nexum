const { SlashCommandBuilder, ModalBuilder, ComponentType, ButtonStyle, ActionRowBuilder, SelectMenuBuilder, EmbedBuilder, ButtonBuilder } = require('discord.js');
const { v4: uuidv4 } = require('uuid');
const profileSchema = require('../../schema/economy/profileSchema.js');
const dragonSchema = require('../../schema/miniGames/dragons.js');

let battles = new Map(); // Map para almacenar información de las batallas activas

module.exports = {
    data: new SlashCommandBuilder()
        .setName('batalla')
        .setDescription('Inicia una batalla de dragones multijugador')
        .addStringOption(option =>
            option.setName('dragon')
                .setDescription('ID del dragón que deseas usar en la batalla')
                .setRequired(true)
        ),

    async execute(client, interaction, profileData) {
        const userId = interaction.user.id;
        const dragonId = interaction.options.getString('dragon');

        const dragon = await dragonSchema.findOne({ _id: dragonId, owner: profileData._id });
        if (!dragon) {
            return interaction.reply({ content: 'No se encontró el dragón o no te pertenece.', ephemeral: true });
        }

        // Generar un ID único para la batalla
        const battleId = uuidv4();

        // Almacenar la información de la batalla en el mapa
        battles.set(battleId, {
            initiator: userId,
            dragonId,
            opponent: null,
            opponentDragonId: null,
        });

        // Construir el embed y los botones para que el oponente acepte el desafío y elija su dragón
        const challengeEmbed = new EmbedBuilder()
            .setTitle('Desafío de Batalla de Dragones')
            .setDescription(`${interaction.user.username} te ha desafiado a una batalla de dragones. ¿Aceptas el desafío?`)
            .setColor(0x00FF00);

        const row = new ActionRowBuilder()
            .addComponents(
                new ButtonBuilder()
                    .setCustomId(`accept_battle_${battleId}`)
                    .setLabel('Aceptar Desafío')
                    .setStyle(ButtonStyle.Primary),
                new ButtonBuilder()
                    .setCustomId(`decline_battle_${battleId}`)
                    .setLabel('Rechazar Desafío')
                    .setStyle(ButtonStyle.Danger)
            );

        await interaction.reply({
            content: '¡Desafío de batalla enviado!',
            embeds: [challengeEmbed],
            components: [row],
            ephemeral: true
        });

        // Función para eliminar la batalla si no es aceptada en un minuto
        setTimeout(() => {
            if (battles.has(battleId)) {
                battles.delete(battleId);
            }
        }, 60000); // 60000 milisegundos = 1 minuto
    },

    async interactionCreate(interaction) {
        if (!interaction.isButton()) return;

        const customId = interaction.customId;
        if (customId.startsWith('accept_battle_')) {
            const battleId = customId.split('_')[2];
            const battle = battles.get(battleId);

            if (!battle || battle.opponent || interaction.user.id === battle.initiator) return;

            const profileData = await profileSchema.findOne({ userId: interaction.user.id });
            if (!profileData) {
                return interaction.reply({ content: 'No tienes un perfil creado.', ephemeral: true });
            }

            // Al usuario que acepta el desafío se le mostrará un menú para elegir su dragón
            const dragons = await dragonSchema.find({ owner: profileData._id, isForSale: false });
            if (dragons.length === 0) {
                return interaction.reply({ content: 'No tienes dragones disponibles para la batalla.', ephemeral: true });
            }

            const options = dragons.map(dragon => ({
                label: dragon.name,
                value: dragon._id.toString(),
            }));

            const row = new ActionRowBuilder()
                .addComponents(
                    new SelectMenuBuilder()
                        .setCustomId(`choose_dragon_${battleId}`)
                        .setPlaceholder('Selecciona tu dragón para la batalla')
                        .addOptions(options)
                );

            await interaction.update({
                content: `${interaction.user.username} ha aceptado el desafío de batalla. Selecciona tu dragón para la batalla.`,
                components: [row],
                ephemeral: true
            });

            // Guardar el ID del oponente en la batalla
            battles.set(battleId, {
                ...battle,
                opponent: interaction.user.id,
            });

        } else if (customId.startsWith('decline_battle_')) {
            const battleId = customId.split('_')[2];
            battles.delete(battleId);
            await interaction.update({ content: 'Has rechazado el desafío de batalla.', components: [], ephemeral: true });

        } else if (customId.startsWith('choose_dragon_')) {
            const battleId = customId.split('_')[2];
            const battle = battles.get(battleId);
            if (!battle || !battle.opponent || interaction.user.id !== battle.opponent) return;

            const chosenDragonId = interaction.values[0];
            const opponentDragon = await dragonSchema.findOne({ _id: chosenDragonId, owner: battle.opponent });

            if (!opponentDragon) {
                return interaction.reply({ content: 'No se encontró el dragón seleccionado o no te pertenece.', ephemeral: true });
            }

            // Almacenar el dragón elegido por el oponente en la batalla
            battles.set(battleId, {
                ...battle,
                opponentDragonId: chosenDragonId,
            });

            await interaction.update({
                content: `¡La batalla entre ${interaction.user.username} y el desafiante ha comenzado!`,
                components: [],
                ephemeral: true
            });

            // Lógica adicional para manejar la batalla puede ser añadida aquí
        }
    }
};
