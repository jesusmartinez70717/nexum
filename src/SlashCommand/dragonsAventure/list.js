const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');
const dragonSchema = require('../../schema/miniGames/dragons.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('listado')
        .setDescription('Muestra el listado de dragones en venta'),

        async execute(client, interaction, profileData) {
        const dragonsForSale = await dragonSchema.find({ isForSale: true });

        if (dragonsForSale.length === 0) {
            return interaction.reply('No hay dragones en venta actualmente.');
        }

        const embed = new EmbedBuilder()
            .setTitle('Dragones en Venta')
            .setDescription('Lista de dragones disponibles para comprar')
            .setColor(0x00FF00);

        dragonsForSale.forEach(dragon => {
            embed.addFields({ name: `${dragon.name} (${dragon.uniqueName})`, value: `Precio: ${dragon.price} Nexis\nAtaque: ${dragon.attack}\nDefensa: ${dragon.defense}\nVelocidad: ${dragon.speed}\nHP: ${dragon.hp}\nHabilidades: ${dragon.abilities.join(', ')}\nRareza: ${dragon.rarity}`, inline: true });
        });

        await interaction.reply({ embeds: [embed] });
    }
};
