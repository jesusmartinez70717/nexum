const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');
const profileSchema = require('../../schema/economy/profileSchema.js');
const dragonSchema = require('../../schema/miniGames/dragons.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('mejorar')
        .setDescription('Mejora a tu dragón')
        .addStringOption(option =>
            option.setName('id')
                .setDescription('ID del dragón que quieres mejorar')
                .setRequired(true)
        ),

        async execute(client, interaction, profileData) {
        const dragonId = interaction.options.getString('id');
        const userId = interaction.user.id;

        const dragon = await dragonSchema.findOne({ _id: dragonId, owner: profileData.userId });
        if (!dragon) {
            return interaction.reply('No se encontró el dragón o no te pertenece.');
        }

        if (profileData.balance < 100) {
            return interaction.reply('No tienes suficientes Nexis para mejorar a tu dragón.');
        }

        // Lógica de mejora del dragón (ejemplo básico, puedes ajustar según tu sistema de juego)
        dragon.level++;
        dragon.attack += 2;
        dragon.defense += 2;
        dragon.speed += 1;
        dragon.hp += 10;

        profileData.balance -= 100;

        await dragon.save();
        await profileData.save();

        const embed = new EmbedBuilder()
            .setTitle(`¡${dragon.name} ha sido mejorado!`)
            .setDescription(`Nuevo nivel: ${dragon.level}\nNuevas estadísticas:\nAtaque: ${dragon.attack}\nDefensa: ${dragon.defense}\nVelocidad: ${dragon.speed}\nHP: ${dragon.hp}`)
            .setColor(0x00FF00);

        await interaction.reply({ embeds: [embed] });
    }
};
