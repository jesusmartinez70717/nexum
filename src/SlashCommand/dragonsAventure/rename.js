const { SlashCommandBuilder } = require('discord.js');
const profileSchema = require('../../schema/economy/profileSchema.js');
const dragonSchema = require('../../schema/miniGames/dragons.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('renombrar')
        .setDescription('Cambia el nombre de tu dragón')
        .addStringOption(option =>
            option.setName('id')
                .setDescription('ID del dragón al que quieres cambiar el nombre')
                .setRequired(true)
        )
        .addStringOption(option =>
            option.setName('nombre')
                .setDescription('Nuevo nombre para tu dragón')
                .setRequired(true)
        ),

        async execute(client, interaction, profileData) {
        const dragonId = interaction.options.getString('id');
        const newDragonName = interaction.options.getString('nombre');
        const userId = interaction.user.id;

        const dragon = await dragonSchema.findOne({ id: dragonId, owner: profileData.userId });
        if (!dragon) {
            return interaction.reply('No se encontró el dragón o no te pertenece.');
        }

        dragon.name = newDragonName;
        await dragon.save();

        await interaction.reply(`¡El nombre de tu dragón ha sido cambiado a ${newDragonName}!`);
    }
};
