const { SlashCommandBuilder, PermissionFlagsBits, EmbedBuilder } = require('discord.js');
const reportSchema = require('../../../db/models/reportModel.js')

module.exports ={ 
    data: new SlashCommandBuilder()
    .setName('report-disable')
    .setDescription('Desactiva  el sistema de reportes en el servidor')
    .setDefaultMemberPermissions(PermissionFlagsBits.Administrator),

    async execute(client, interaction) {
        const { channel, guildId, options } = interaction
        const repChannel = options.getChannel('canal')

        const embed = new EmbedBuilder()

        const data = reportSchema.deleteMany({Guild: guildId})
        if(data)
        embed.setColor('Red')
        .setDescription('Su sistema de reporte ha sido removido')
        await interaction.reply({embeds: [embed], ephemeral: true})
    }
}