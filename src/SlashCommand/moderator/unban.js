const { SlashCommandBuilder, PermissionFlagsBits, EmbedBuilder} = require('discord.js');

module.exports = {
    data: new SlashCommandBuilder()
    .setName('unban')
    .setDescription('Le quita el ban a un usuario')
    .addStringOption(option => option.setName('id').setDescription('El id del usuario baneado').setRequired(true))
    .addStringOption(option => option.setName('razon').setDescription('La razon del desbaneo').setRequired(false))
    .setDefaultMemberPermissions(PermissionFlagsBits.BanMembers),
    async execute(client, interaction){
        const id = interaction.options.getString('id');
        const reason = interaction.options.getString('razon') || 'No se proporcionó una razón'

        const embed = new EmbedBuilder()
        .setTitle('Desbaneo')
        .setColor('Green')
        .setDescription(`El usuario con el id ${id} ha sido desbaneado | ${reason}`)

        interaction.guild.bans.fetch()
        .then(async bans => {
            if(bans.size == 0) return interaction.reply({content: 'No hay nadie baneado en este servidor', ephemeral: true})

            let bannedId = bans.find(ban => ban.user.id == id)
            if(!bannedId) return await interaction.reply({content: 'Ese usuario no se encuentra baneado en el servidor', ephemeral: true})

            await interaction.guild.bans.remove(id, reason).catch(err => {
                return interaction.reply({content: 'Ocurrio un error al querer desbanear al usuario', ephemeral: true})
            })
            await interaction.reply({embeds: [embed]})
        })
    }
}