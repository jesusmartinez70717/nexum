const { EmbedBuilder,SlashCommandBuilder } = require('discord.js')

module.exports = {
    data: new SlashCommandBuilder()
    .setName('ban-history')
    .setDescription('Te da el historial de baneos del usuario mencionado')
    .addUserOption(option => option.setName('usuario').setDescription('El usuario del que se mostrara el historial').setRequired(true)),
    async execute(client, interaction) {
        const user = interaction.options.getUser('usuario')

        await interaction.deferReply({ephemeral: true})

        async function sendMessage(message) {
            const embed = new EmbedBuilder()
            .setColor('Red')
            .setDescription(message)

            await interaction.editReply({embeds: [embed]})
        }
        let guilds = await client.guilds.fetch();
        var banString = ``;
        
        for(const [id, guild] of guilds) {
            let fetchedGuild = await client.guilds.fetch(id);
            const bans = await fetchedGuild.bans.fetch();

            let fetchedBan = bans.get(user.id)
            if(fetchedBan) banString += `> Guild: ${fetchedBan.guild.id}\n> Razon Baneo: \`${fetchedBan.reazon}\`\n\n`
        }

        if(banString.length == 0) {
            await sendMessage(`\`${user.username}\` No tiene ningun baneo en el servidor`)
        } else {
            await sendMessage(`${user.username} (\`${user.id}\`) Tiene este historial de ban: \n\n${banString}`)
        }
    }
}