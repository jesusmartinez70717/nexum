const { SlashCommandBuilder, PermissionFlagsBits, EmbedBuilder } = require('discord.js');

module.exports = {
    data: new SlashCommandBuilder()
    .setName('unlock')
    .setDescription('Desbloquea un canal del servidor')
    .addChannelOption(option => option.setName('canal').setDescription('El canal que quieres desbloquear').setRequired(true))
    .setDefaultMemberPermissions(PermissionFlagsBits.ManageChannels),

    async execute(client, interaction) {
    
        let channel = interaction.options.getChannel('canal')

        channel.permissionOverwrites.create(interaction.guild.id, {SendMessages: true})

        const embed = new EmbedBuilder()
        .setColor('Red')
        .setDescription(`Èl canal ${channel} se ha desbloqueado`)

        await interaction.reply({embeds: [embed]})
    }
}