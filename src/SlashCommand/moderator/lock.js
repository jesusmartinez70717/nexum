const { SlashCommandBuilder, PermissionFlagsBits, EmbedBuilder } = require('discord.js');

module.exports = {
    data: new SlashCommandBuilder()
    .setName('lock')
    .setDescription('Bloque un canal del servidor')
    .addChannelOption(option => option.setName('canal').setDescription('El canal que quieres bloquea').setRequired(true))
    .setDefaultMemberPermissions(PermissionFlagsBits.ManageChannels),

    async execute(client, interaction) {
    
        let channel = interaction.options.getChannel('canal')

        channel.permissionOverwrites.create(interaction.guild.id, {SendMessages: false})

        const embed = new EmbedBuilder()
        .setColor('Red')
        .setDescription(`Èl canal ${channel} se ha bloqueado`)

        await interaction.reply({embeds: [embed]})
    }
}