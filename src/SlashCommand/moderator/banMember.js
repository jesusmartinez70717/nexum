const { SlashCommandBuilder, EmbedBuilder, PermissionFlagsBits} = require('discord.js')

module.exports = {
    data: new SlashCommandBuilder()
    .setName('ban')
    .setDescription('Banea a un miembro del servidor')
    .addUserOption(Option => Option.setName('usuario').setDescription('Usuario que quieres expulsar').setRequired(true))
    .addStringOption(option => option.setName('razon').setDescription('La razon del baneo').setRequired(true))
    .setDefaultMemberPermissions(PermissionFlagsBits.BanMembers),

    async execute(client, interaction) {
        const user = interaction.options.getUser('usuario')
        const reason = interaction.options.getString('razon')
        const miembro = await interaction.guild.members.fetch(user.id);
        const dmEmbed = new EmbedBuilder()
        .setTitle('Baneado')
        .setDescription(`:white_check_mark:  Has sido **Baneado** de ${interaction.guild.name} | ${reason}`)
        .setColor('Blue')

        const embed = new EmbedBuilder()
        .setTitle('Baneado')
        .setDescription(`:white_check_mark: ${user} Ha sido **Baneado** del servidor | ${reason}`)
        .setColor('Blue')

        miembro.ban({reason: reason}).catch(err => {interaction.reply({content: 'Ocurrio un error al querer banear al usuario', ephemeral: true})})
        interaction.reply({content: 'El usuario ha sido baneado', ephemeral: true})
        user.send({embeds: [dmEmbed]}).catch(err => {})
        interaction.channel.send({embeds: [embed]})
    }
}