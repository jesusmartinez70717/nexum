const { SlashCommandBuilder, PermissionFlagsBits, EmbedBuilder } = require('discord.js');
const warningSchema = require('../../../db/models/warningModel.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('cleanwarn')
        .setDescription('Elimina todos los avisos de un usuario')
        .addUserOption(option => option.setName('usuario').setDescription('El usuario cuyos avisos deseas eliminar').setRequired(true))
        .setDefaultMemberPermissions(PermissionFlagsBits.KickMembers),

    async execute(client, interaction) {
        const { options, guildId } = interaction;
        const target = options.getUser('usuario');

        try {
            const result = await warningSchema.findOneAndDelete({ GuildID: guildId, UserID: target.id, UserTag: target.tag });

            if (result) {
                const embed = new EmbedBuilder()
                    .setColor('Green')
                    .setDescription(`Todos los avisos de ${target.tag} han sido eliminados.`);

                await interaction.reply({ embeds: [embed], ephemeral: true });
            } else {
                const noWarningsEmbed = new EmbedBuilder()
                    .setColor('Red')
                    .setDescription(`${target.tag} no tiene ningún aviso para eliminar.`);

                await interaction.reply({ embeds: [noWarningsEmbed], ephemeral: true });
            }
        } catch (err) {
            console.error('Error al ejecutar el comando cleanwarn:', err);
            await interaction.reply({ content: 'Hubo un error al ejecutar el comando.', ephemeral: true });
        }
    }
};
