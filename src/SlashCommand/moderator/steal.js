const { SlashCommandBuilder, PermissionFlagsBits, parseEmoji, EmbedBuilder} = require('discord.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('steal')
        .setDescription('Sube a tu servidor el stiker pasado')
        .addStringOption(option => option.setName('emoji').setDescription('El stiker').setRequired(true))
        .addStringOption(option => option.setName('nombre').setDescription('El nombre con el que se guardara el emoji').setRequired(false))
        .setDefaultMemberPermissions(PermissionFlagsBits.ManageGuildExpressions),
    async execute(client, interaction) {
        const emoji = parseEmoji(interaction.options.getString('emoji'));
        if (!emoji) return interaction.reply({ content: 'No se ha proporcionado un archivo de emoji'})
        if(emoji.id) {
            const extension = emoji.animated ? '.gif' : '.png'
            const url = `https://cdn.discordapp.com/emojis/${emoji.id}${extension}`
            const emojiName = interaction.options.getString('nombre') || emoji.name

            interaction.guild.emojis
            .create({
                attachment: url, 
                name: emojiName
            }).then(emoji => {
                const embed = new EmbedBuilder()
                .setTitle('Emoji creado')
                .setDescription(`Se ha añadido al servidor el emoji ${emoji} con el nombre de **${emojiName}**`)
                .setColor('Green')
                interaction.reply({ embeds: [embed] })
            }).catch((err) => {
                interaction.reply({content: 'ocurrio un error al agregar el emoji, verifica que no hallan alcanzado el limite de emojis permitidos', ephemeral: true})
            });
        }
    }
};
