const { SlashCommandBuilder, PermissionFlagsBits } = require('discord.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('mute')
        .setDescription('Aisla a un usuario por un periodo de tiempo especificado')
        .addUserOption(option => 
            option.setName('usuario')
                .setDescription('El usuario que será aislado')
                .setRequired(true))
        .addStringOption(option => 
            option.setName('duracion')
                .setDescription('La duración del aislamiento')
                .setRequired(false)
                .addChoices(
                    { name: '10 minutos', value: '10m' },
                    { name: '1 hora', value: '1h' },
                    { name: '2 horas', value: '2h' },
                    { name: '8 horas', value: '8h' },
                    { name: '1 día', value: '1d' },
                    { name: 'quitar aislamiento', value: 'forever' }
                ))
        .setDefaultMemberPermissions(PermissionFlagsBits.ModerateMembers),
    async execute(client, interaction) {
        const usuario = interaction.options.getUser('usuario');
        const duracion = interaction.options.getString('duracion') || '10m';

        const miembro = await interaction.guild.members.fetch(usuario.id);
        let muteUntil = null; // default is forever

        if (duracion !== 'forever') {
            muteUntil = new Date();
            switch (duracion) {
                case '10m':
                    muteUntil.setMinutes(muteUntil.getMinutes() + 10);
                    break;
                case '1h':
                    muteUntil.setHours(muteUntil.getHours() + 1);
                    break;
                case '2h':
                    muteUntil.setHours(muteUntil.getHours() + 2);
                    break;
                case '8h':
                    muteUntil.setHours(muteUntil.getHours() + 8);
                    break;
                case '1d':
                    muteUntil.setDate(muteUntil.getDate() + 1);
                    break;
            }
        }

        try {
            await miembro.disableCommunicationUntil(muteUntil, `Muteado por ${duracion}`);

            const durationText = duracion === 'forever' ? 'para siempre' : `por ${duracion}`;
            interaction.reply({ content: `${usuario.tag} ha sido silenciado ${durationText}.`, ephemeral: true });

        } catch (err) {
            console.error(err);
            interaction.reply({ content: `No se pudo silenciar a ${usuario.tag}.`, ephemeral: true });
        }
    }
};
