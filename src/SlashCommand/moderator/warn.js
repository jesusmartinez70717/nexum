const { SlashCommandBuilder, EmbedBuilder, PermissionFlagsBits } = require('discord.js');
const warningSchema = require('../../../db/models/warningModel.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('warn')
        .setDescription('Le da un aviso al integrante que quieres')
        .addUserOption(option => option.setName('usuario').setDescription('El usuario al que le quieres avisar').setRequired(true))
        .addStringOption(option => option.setName('razon').setDescription('La razón del aviso').setRequired(false))
        .setDefaultMemberPermissions(PermissionFlagsBits.KickMembers),

    async execute(client, interaction) {
        const { options, guildId, user } = interaction;
        const target = options.getUser('usuario');
        const reason = options.getString('razon') || 'No se especificó una razón';
        
        try {
            let data = await warningSchema.findOne({ GuildID: guildId, UserID: target.id, UserTag: target.tag});

            if (!data) {
                data = new warningSchema({
                    GuildID: guildId,
                    UserID: target.id,
                    UserTag: target.tag,
                    Content: [
                        {
                            ExecuterId: user.id,
                            ExecuterTag: user.tag,
                            Reazon: reason
                        }
                    ]
                });
            } else {
                const warnContent = {
                    ExecuterId: user.id,
                    ExecuterTag: user.tag,
                    Reazon: reason
                };
                data.Content.push(warnContent);
            }

            await data.save();

            const dmEmbed = new EmbedBuilder()
                .setColor('Blue')
                .setDescription(`Has sido advertido en ${interaction.guild.name} | ${reason}`);

            const embed = new EmbedBuilder()
                .setColor('Blue')
                .setDescription(`${target.tag} ha sido advertido | ${reason}`);
            
            await target.send({ embeds: [dmEmbed] }).catch(err => {});
            await interaction.reply({ embeds: [embed], ephemeral: true });

        } catch (err) {
            console.error('Error al ejecutar el comando warn:', err);
            await interaction.reply({ content: 'Hubo un error al ejecutar el comando.', ephemeral: true });
        }
    }
};
