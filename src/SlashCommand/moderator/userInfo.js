const { SlashCommandBuilder, PermissionFlagsBits, EmbedBuilder} = require('discord.js');
const { parse } = require('path');

module.exports = {
    data: new SlashCommandBuilder()
    .setName('whois')
    .setDescription('Da informacion basica de un usuario')
    .addUserOption(option => option.setName('usuario').setDescription('El usuario del que se proporcionara infomracion').setRequired(false)),
    async execute(client, interaction){
        const user = interaction.options.getUser('usuario') || interaction.user;
        const member = await interaction.guild.members.fetch(user.id)
        const icon = user.displayAvatarURL()
        const tag = user.tag
        const embed = new EmbedBuilder()
        .setColor('Red')
        .setAuthor({name: tag, iconURL: icon})
        .setThumbnail(icon)
        .addFields(
            {name: 'Miembro', value: `${user}`, inline: false},
            {name: 'Roles', value: `${member.roles.cache.map(r => r).join(' ')}`, inline: false},
            {name: 'Union al servidor', value: `<t:${parseInt(member.joinedAt / 1000)}:R>`, inline: true},
            {name: 'Union a discord', value: `<t:${parseInt(user.createdAt / 1000)}:R>`, inline:true}
        )
        .setFooter({text: `User ID: ${member.id}`})
        .setTimestamp()

        interaction.reply({embeds: [embed]})
    }
}