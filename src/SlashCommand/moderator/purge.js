const { SlashCommandBuilder, PermissionFlagsBits } = require('discord.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('purge')
        .setDescription('Elimina mensajes del canal')
        .addNumberOption(option => option.setName('cantidad').setDescription('La cantidad de mensajes que se eliminarán').setRequired(true))
        .setDefaultMemberPermissions(PermissionFlagsBits.ManageMessages),
    async execute(client, interaction) {
        const cantidad = interaction.options.getNumber('cantidad');
        if (cantidad < 1) return interaction.reply({ content: 'Debes ingresar una cantidad mayor a 0', ephemeral: true });

        let mensajesEliminados = 0;
        let mensajesRestantes = cantidad;

        while (mensajesRestantes > 0) {
            const toDelete = Math.min(mensajesRestantes, 99);
            try {
                const deletedMessages = await interaction.channel.bulkDelete(toDelete, true); // true para filtrar mensajes mayores a 14 días
                mensajesEliminados += deletedMessages.size;
                mensajesRestantes -= deletedMessages.size;
                if (deletedMessages.size < toDelete) break; // No hay más mensajes para eliminar
            } catch (err) {
                console.error(err);
                break;
            }
        }

        interaction.reply({ content: `Se han eliminado ${mensajesEliminados} mensajes`, ephemeral: true }).catch(err => {});
    }
};
