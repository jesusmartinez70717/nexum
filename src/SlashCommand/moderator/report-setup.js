const { SlashCommandBuilder, PermissionFlagsBits, EmbedBuilder, ChannelType } = require('discord.js');
const reportSchema = require('../../../db/models/reportModel.js')

module.exports = { 
    data: new SlashCommandBuilder()
    .setName('report-setup')
    .setDescription('Configura el sistema de reportes en el servidor')
    .addChannelOption(option => option.setName('canal').setDescription('El canal al que se enviaran los nuevos reportes').addChannelTypes(ChannelType.GuildText).setRequired(true))
    .setDefaultMemberPermissions(PermissionFlagsBits.Administrator),

    async execute(client, interaction) {
        const { channel, guildId, options } = interaction
        const repChannel = options.getChannel('canal')

        const embed = new EmbedBuilder()

        const data = await reportSchema.findOne({Guild: guildId})
        if(!data) {
            await reportSchema.create({
                Guild: guildId,
                Channel: repChannel.id
            })
            embed.setColor('Red')
            .setDescription(`Todos los reportes se enviaran a ${repChannel}`)
        } else if (data) {
            const c = client.channels.cache.get(data.Channel);
            embed.setColor('Red')
            .setDescription(`Los reportes se estan enviando a ${c}`)
        }
        return await interaction.reply({embeds: [embed], ephemeral: true})
    }
}