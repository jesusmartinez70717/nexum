const { SlashCommandBuilder, EmbedBuilder, ChannelType, Guild } = require('discord.js')
const log = require('../../../db/models/deleteMessageLogModel.js')

module.exports = {
    data: new SlashCommandBuilder()
    .setName('delete-message-log')
    .setDescription('Configura el sistema de logs de mensajes eliminados')
    .addSubcommand(command => command.setName('setup').setDescription('Configura el sistema de log').addChannelOption(option => option.setName('canal').setDescription('El canal al que se enviaran los avisos').addChannelTypes(ChannelType.GuildText).setRequired(true)))
    .addSubcommand(command => command.setName('desactivar').setDescription('Desactiva el sistema de logs de mensajes eliminados')),
    async execute(client, interaction) {
        const { options } = interaction;
        const sub = options.getSubcommand()
        let data = await log.findOne({Guild: interaction.guild.id})

        async function sendMessage(message) {
            const embed = new EmbedBuilder()
            .setColor('Red')
            .setDescription(message)

            await interaction.reply({embeds: [embed], ephemeral: true})
        }
        switch (sub) {
            case 'setup':
                if(data) return await sendMessage('Ya existe una configuracion de sistema')
                let channel = options.getChannel('canal')
            await log.create({
                Guild: interaction.guild.id,
                Channel: channel.id
            })
            await sendMessage(`Se ha configurado el canal de logs para el sistema en ${channel}`)
                break;
                case 'desactivar':
                if(!data) return await sendMessage('No existe ninguna configuracion para el sistema de logs de mensajes eliminados')
                await log.deleteOne({Guild: interaction.guild.id})
            await sendMessage('Se ha eliminado la configuracion del sistema')
                break;
        }
    }
}