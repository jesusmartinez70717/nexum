const { SlashCommandBuilder, PermissionFlagsBits, EmbedBuilder } = require('discord.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('nickname')
        .setDescription('Cambia el apodo de un usuario')
        .addUserOption(option => 
            option.setName('usuario')
                .setDescription('El usuario al que se le cambiará el apodo')
                .setRequired(true))
        .addStringOption(option => 
            option.setName('apodo')
                .setDescription('El nuevo apodo')
                .setRequired(true))
        .setDefaultMemberPermissions(PermissionFlagsBits.ManageNicknames),
    async execute(client, interaction) {
        const user = interaction.options.getUser('usuario');
        const name = interaction.options.getString('apodo');

        // Obtener el miembro del servidor (GuildMember)
        const miembro = await interaction.guild.members.fetch(user.id);

        try {
            await miembro.setNickname(name);
            const embed = new EmbedBuilder()
            .setTitle('Nombre cambiado')
            .setDescription(`:white_check_mark: el nombre de ${user.username} ha sido cambiado a ${name}`)
            .setColor('Blue')

            interaction.reply({embeds: [embed], ephemeral: true})

        } catch (err) {
            console.error(err);
            interaction.reply({ content: `No se pudo cambiar el apodo de ${user}.`, ephemeral: true });
        }
    }
};
