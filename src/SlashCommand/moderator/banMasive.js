const { SlashCommandBuilder } = require('discord.js');
const { PermissionFlagsBits } = require('discord.js');

const data = new SlashCommandBuilder()
    .setName('banmasivo')
    .setDefaultMemberPermissions(PermissionFlagsBits.Administrator)
    .setDescription('Banea a uno o varios usuarios del servidor.')
    .addStringOption(option => option.setName('usuarios').setDescription('La lista de usuarios que serán baneados.').setRequired(true))
    .addStringOption(option => option.setName('razon').setDescription('La razón del baneo.').setRequired(false));

async function execute(client, interaction) {
    const rawUserIds = interaction.options.getString('usuarios').split(','); // Obtener lista de usuarios separados por comas
    const userIds = rawUserIds.map(id => id.trim().replace(/[<@!>]/g, '')); // Limpiar IDs para asegurar que son solo números
    const razon = interaction.options.getString('razon') || 'No especificada';

    const guild = interaction.guild;

    for (const userId of userIds) {
        try {
            const member = await guild.members.fetch(userId);
            if (member) {
                await member.ban({ reason: razon }).catch(err => {});
            } else {
                await interaction.followUp(`No se pudo encontrar al usuario con ID: ${userId}`);
            }
        } catch (error) {
            console.error(`Error al intentar banear al usuario con ID: ${userId}`, error);
            await interaction.followUp(`Hubo un error al intentar banear al usuario con ID: ${userId}`);
        }
    }

    await interaction.reply(`Usuarios ${userIds.join(', ')} baneados por: ${razon}`);
}

module.exports = {
  data,
  execute
};
