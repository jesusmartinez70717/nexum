const { SlashCommandBuilder } = require('@discordjs/builders');
const AntiRaidConfig = require('../../../db/models/antiraidModel.js'); // Importa el modelo que definiste anteriormente
const { ChannelType } = require('discord.js');
module.exports = {
  data: new SlashCommandBuilder()
    .setName('antiraidconfig')
    .setDescription('Configura el sistema AntiRaid')
    .addBooleanOption(option =>
      option
        .setName('detectarmensajes')
        .setDescription('Activa/desactiva la detección de envío masivo de mensajes.')
        .setRequired(true)
    )
    .addBooleanOption(option =>
      option
        .setName('detectarcrearcanal')
        .setDescription('Activa/desactiva la detección de creación masiva de canales.')
        .setRequired(true)
    )
    .addStringOption(option =>
      option
        .setName('mensajes')
        .setDescription('Cantidad de mensajes permitidos por minuto.')
        .setRequired(true)
    )
    .addStringOption(option =>
      option
        .setName('accion')
        .setDescription('Selecciona la acción a realizar en caso de detectar una actividad de raid.')
        .setRequired(true)
        .addChoices([
          { name: 'Advertencia', value: 'warn' },
          { name: 'Silenciar', value: 'mute' },
          { name: 'Expulsar', value: 'kick' },
          { name: 'Banear', value: 'ban' },
          { name: 'Ninguna', value: 'none' }
        ])
    )
    .addChannelOption(option =>
      option
        .setName('canallogs')
        .setDescription('Selecciona el canal donde se registrarán las acciones del sistema AntiRaid.')
        .setRequired(true)
        .addChannelTypes(ChannelType.GuildText)
    ),
  async execute(client, interaction) {
    const guildId = interaction.guild.id;
    const detectarMensajes = interaction.options.getBoolean('detectarmensajes');
    const detectarCrearCanal = interaction.options.getBoolean('detectarcrearcanal');
    const accion = interaction.options.getString('accion');
    const mensajes = parseInt(interaction.options.getString('mensajes')); // Parsea a número
    const canalLogsId = interaction.options.getChannel('canallogs').id;

    try {
      // Busca si ya existe una configuración para este guild
      let config = await AntiRaidConfig.findOne({ guildId });

      if (!config) {
        // Si no existe, crea un nuevo documento
        config = new AntiRaidConfig({
          guildId,
          detectarMensajes,
          detectarCrearCanal,
          accion,
          mensajes,
          canalLogsId
        });
      } else {
        // Si ya existe, actualiza los campos
        config.detectarMensajes = detectarMensajes;
        config.detectarCrearCanal = detectarCrearCanal;
        config.accion = accion;
        config.mensajes = mensajes;
        config.canalLogsId = canalLogsId;
      }

      // Guarda la configuración en la base de datos
      await config.save();

      // Responde al usuario con la configuración actualizada
      await interaction.reply(`La configuración del sistema AntiRaid ha sido actualizada:
      - Detectar envío masivo de mensajes: ${detectarMensajes ? 'Activado' : 'Desactivado'}
      - Detectar creación masiva de canales: ${detectarCrearCanal ? 'Activado' : 'Desactivado'}
      - Acción a realizar: ${accion}
      - Mensajes permitidos por minuto: ${mensajes}
      - Canal de logs: <#${canalLogsId}>`);

    } catch (error) {
      console.error('Error al configurar AntiRaid:', error);
      await interaction.reply('Ocurrió un error al configurar el sistema AntiRaid.');
    }
  },
};
