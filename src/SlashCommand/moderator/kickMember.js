const { SlashCommandBuilder, EmbedBuilder, PermissionFlagsBits} = require('discord.js')

module.exports = {
    data: new SlashCommandBuilder()
    .setName('kick')
    .setDescription('Expulsa a un miembro del servidor')
    .addUserOption(Option => Option.setName('usuario').setDescription('Usuario que quieres expulsar').setRequired(true))
    .addStringOption(option => option.setName('razon').setDescription('La razon de la expulsion').setRequired(true))
    .setDefaultMemberPermissions(PermissionFlagsBits.KickMembers),

    async execute(client, interaction) {
        const user = interaction.options.getUser('usuario')
        const member = await interaction.guild.members.fetch(user.id);
        const reason = interaction.options.getString('razon')

        const dmEmbed = new EmbedBuilder()
        .setTitle('Expulsado')
        .setDescription(`:white_check_mark:  Has sido **expulsado** de ${interaction.guild.name} | ${reason}`)
        .setColor('Blue')

        const embed = new EmbedBuilder()
        .setTitle('Expulsado')
        .setDescription(`:white_check_mark: ${user} Ha sido **expulsado** del servidor | ${reason}`)
        .setColor('Blue')

        member.kick({reason: reason}).catch(err => {interaction.reply({content: 'Ocurrio un error al querer expulsar al usuario', ephemeral: true})})
        interaction.reply({content: 'El usuario ha sido expulsado', ephemeral: true})
        user.send({embeds: [dmEmbed]}).catch(err => {})
        interaction.channel.send({embeds: [embed]})
    }
}