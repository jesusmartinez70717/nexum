const { SlashCommandBuilder, PermissionFlagsBits } = require('discord.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('timeout')
        .setDescription('Silencia a un usuario por un periodo de tiempo especificado')
        .addUserOption(option => 
            option.setName('usuario')
                .setDescription('El usuario que será silenciado')
                .setRequired(true))
        .addStringOption(option => 
            option.setName('duracion')
                .setDescription('La duración del mute (ej: 10m, 1h, 1d)')
                .setRequired(true))
        .setDefaultMemberPermissions(PermissionFlagsBits.ModerateMembers),
    async execute(client, interaction) {
        const usuario = interaction.options.getUser('usuario');
        const duracion = interaction.options.getString('duracion');

        const miembro = await interaction.guild.members.fetch(usuario.id);
        let muteUntil = new Date();

        const durationPattern = /^(\d+)([mhd])$/;
        const match = duracion.match(durationPattern);

        if (!match) {
            return interaction.reply({ content: 'Formato de duración inválido. Use m (minutos), h (horas) o d (días).', ephemeral: true });
        }

        const amount = parseInt(match[1]);
        const unit = match[2];

        switch (unit) {
            case 'm':
                muteUntil.setMinutes(muteUntil.getMinutes() + amount);
                break;
            case 'h':
                muteUntil.setHours(muteUntil.getHours() + amount);
                break;
            case 'd':
                muteUntil.setDate(muteUntil.getDate() + amount);
                break;
        }

        try {
            await miembro.timeout(muteUntil, `Muteado por ${duracion}`);

            interaction.reply({ content: `${usuario.tag} ha sido silenciado por ${duracion}.`, ephemeral: true });

            setTimeout(async () => {
                try {
                    await miembro.timeout(null, 'Desmuteo automático');
                    await usuario.send(`Has sido desmuteado en ${interaction.guild.name}.`);
                } catch (err) {
                    console.error(`No se pudo enviar un mensaje a ${usuario.tag}: ${err}`);
                }
            }, muteUntil.getTime() - Date.now());

        } catch (err) {
            console.error(err);
            interaction.reply({ content: `No se pudo silenciar a ${usuario.tag}.`, ephemeral: true });
        }
    }
};
