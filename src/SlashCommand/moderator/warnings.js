const { SlashCommandBuilder, EmbedBuilder, PermissionFlagsBits } = require('discord.js');
const warningSchema = require('../../../db/models/warningModel.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('warnings')
        .setDescription('Puedes ver cuántos avisos tiene un usuario')
        .addUserOption(option => option.setName('usuario').setDescription('El usuario del que quieres ver los avisos').setRequired(true)),
        
    async execute(client, interaction) {
        const { options, guildId } = interaction;
        const target = options.getUser('usuario');
        const embed = new EmbedBuilder();
        const noWarns = new EmbedBuilder().setColor('Red').setDescription(`${target.tag} no tiene ningún aviso.`);

        try {
            let data = await warningSchema.findOne({ GuildID: guildId, UserID: target.id, UserTag: target.tag});

            if (data && data.Content.length > 0) {
                let warningsDescription = data.Content.map(
                    (w, i) => `**Aviso ${i + 1}**\n**Moderador**: ${w.ExecuterTag}\n**Razón**: ${w.Reazon}`
                ).join('\n\n');
                
                embed.setColor('Blue')
                     .setTitle(`Avisos de ${target.tag}`)
                     .setDescription(warningsDescription);
                
                await interaction.reply({ embeds: [embed] });
            } else {
                await interaction.reply({ embeds: [noWarns] });
            }
        } catch (err) {
            console.error('Error al ejecutar el comando warnings:', err);
            await interaction.reply({ content: 'Hubo un error al ejecutar el comando.', ephemeral: true });
        }
    }
};
