const { EmbedBuilder,SlashCommandBuilder } = require('discord.js')
require('dotenv').config()
const parseMilliseconds = require('parse-ms-2')
const profileSchema = require('../../../db/models/economy/profileModel')
const { dailyMin, dailyMax } = require('../../../config/config')
module.exports = {
    data: new SlashCommandBuilder()
    .setName('daily')
    .setDescription('Reclama tus Nexis diarios'),
    async execute(client, interaction, profileData) {
        const { id } = interaction.user;
        const { dailyLastUsed } = profileData;

        const cooldown = 86400000
        const timeLeft = cooldown - (Date.now() - dailyLastUsed)

        if(timeLeft > 0) {
            await interaction.deferReply({ephemeral: true});
            const { hours, minutes, seconds } = parseMilliseconds(timeLeft)
            await interaction.editReply(`Puedes reclamar los proximos Nexis diarios en: ${hours} hrs ${minutes} min ${seconds} sec`)
        }
        await interaction.deferReply({ephemeral: true});
        const randomAmt = Math.floor(Math.random() * (dailyMax - dailyMin + 1) + dailyMin)
        await profileSchema.findOneAndUpdate(
            { userId: id},
            {
                $set: {
                    dailyLastUsed: Date.now()
                },
                $inc: {
                    balance: randomAmt
                }
            }
        ).catch(err => {})
        const embed = new EmbedBuilder()
        .setColor('Red')
        .setDescription(`Haz reclamado ${randomAmt} Nexis, vuelve mañana y reclama de nuevo`)

        await interaction.editReply({embeds: [embed]})
    }
}