const { SlashCommandBuilder, ActionRowBuilder, ButtonBuilder, ButtonStyle } = require('discord.js');
const profileSchema = require('../../../db/models/economy/profileModel')

module.exports = {
    data: new SlashCommandBuilder()
        .setName('dados')
        .setDescription('Juega a los dados y puedes ganar o perder Nexis')
        .addIntegerOption(option => option.setName('apuesta').setDescription('Cantidad a apostar').setRequired(true)),
    async execute(client, interaction, profileData) {
        const { id } = interaction.user;
        const apuesta = interaction.options.getInteger('apuesta');
        const { balance } = profileData;

        // Verifica si el usuario tiene suficiente saldo
        if (balance < apuesta) {
            return interaction.reply(`No tienes suficiente saldo para apostar ${apuesta} Nexis.`);
        }

        const row = new ActionRowBuilder()
            .addComponents(
                new ButtonBuilder()
                    .setCustomId('jugar')
                    .setLabel('Lanzar Dados')
                    .setStyle(ButtonStyle.Primary)
            );

        await interaction.reply({
            content: `¡Apuesta realizada! Haz clic en "Lanzar Dados" para jugar.`,
            components: [row]
        });

        const filter = i => i.customId === 'jugar' && i.user.id === interaction.user.id;
        const collector = interaction.channel.createMessageComponentCollector({ filter, time: 60000 });

        collector.on('collect', async i => {
            const resultadoJugador = Math.floor(Math.random() * 6) + 1;
            const resultadoBot = Math.floor(Math.random() * 6) + 1;

            let mensaje = `Tú lanzaste: ${resultadoJugador}\nEl bot lanzó: ${resultadoBot}\n`;

            if (resultadoJugador > resultadoBot) {
                await profileSchema.findOneAndUpdate({ userId: id }, { $inc: { balance: apuesta } });
                mensaje += `¡Ganaste ${apuesta} Nexis!`;
            } else if (resultadoJugador < resultadoBot) {
                await profileSchema.findOneAndUpdate({ userId: id }, { $inc: { balance: -apuesta } });
                mensaje += `Perdiste ${apuesta} Nexis.`;
            } else {
                mensaje += `Empate. No ganas ni pierdes Nexis.`;
            }

            await i.update({ content: mensaje, components: [] });
        });

        collector.on('end', collected => {
            if (collected.size === 0) {
                interaction.editReply({ content: 'No lanzaste los dados a tiempo. Juego cancelado.', components: [] });
            }
        });
    }
};
