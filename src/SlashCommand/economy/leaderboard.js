const { EmbedBuilder,SlashCommandBuilder } = require('discord.js')
const profileSchema = require('../../../db/models/economy/profileModel')
module.exports = {
    data: new SlashCommandBuilder()
    .setName('leaderboard')
    .setDescription('Muestra los 10 usuarios con mas Nexis'),
    async execute(client, interaction, profileData) {
        await interaction.deferReply({ephemeral: false})
        const { username, id } = interaction.user;
        const { balance } = profileData;
        let leaderboard = new EmbedBuilder()
        .setColor('Red')
        .setTitle('**Top 10 usuarios con mas Nexis**')
        .setFooter({text: 'aún no estás clasificado'})

        const members = await profileSchema
        .find()
        .sort({balance: -1})
        .catch(err => {})

        const memberIdx = members.findIndex((member) => member.userId === id)
        leaderboard.setFooter({text: `estás en el puesto #${memberIdx + 1} con ${balance}`})
        const topTen = members.slice(0, 10)

        let desc = '';
        for(let i = 0; i < topTen.length; i++) {
            let { user } = await interaction.guild.members.fetch(topTen[i].userId)
            if(!user) return
            let userBalance = topTen[i].balance
            desc += `**${i + 1}. ${user.username}:** ${userBalance} Nexis\n`
        }
        if(desc !== '') leaderboard.setDescription(desc)
        await interaction.editReply({embeds: [leaderboard]})
    }
}