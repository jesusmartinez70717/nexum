const { SlashCommandBuilder } = require('discord.js')
const profileSchema = require('../../../db/models/economy/profileModel')

module.exports = {
    owner: true,
    data: new SlashCommandBuilder()
    .setName('admin')
    .setDescription('Parte de administrador de economia')
    .addSubcommand(command => command.setName('agregar').setDescription('Añadir Nexis a un usuario').addUserOption(option => option.setName('usuario').setDescription('El usuario al que se le añadiran los nexis').setRequired(true)).addIntegerOption(option => option.setName('nexis').setDescription('Los nexis que se le añadiran').setRequired(true).setMinValue(1)))
    .addSubcommand(command => command.setName('quitar').setDescription('Quitar Nexis a un usuario').addUserOption(option => option.setName('usuario').setDescription('El usuario al que se le quitaran los nexis').setRequired(true)).addIntegerOption(option => option.setName('nexis').setDescription('Los nexis que se le quitaran').setRequired(true).setMinValue(1))),
    async execute(client, interaction, profileData) {
        const sub = interaction.options.getSubcommand()
        const user = interaction.options.getUser('usuario')
        const nexis = interaction.options.getInteger('nexis')

        switch (sub) {
            case 'agregar':
                await profileSchema.findOneAndUpdate(
                    {userId: user.id},
                    {
                        $inc: {balance: nexis}
                    }
                )
            await interaction.reply({content: `Se han añadido ${nexis} nexis a ${user.username}`})
                break;
        
            case 'quitar':
                await profileSchema.findOneAndUpdate(
                    {userId: user.id},
                    {
                        $inc: {balance: -nexis}
                    }
                )
                await interaction.reply({content: `Se han quitado ${nexis} nexis a ${user.username}`})
                break;
        }
    }}