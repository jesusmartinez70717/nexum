const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');
const profileSchema = require('../../../db/models/economy/profileModel')
require('dotenv').config();

const roles = {
    vip: { name: 'VIP', cost: 100 },
    premium: { name: 'Premium', cost: 250 },
    elite: { name: 'Elite', cost: 500 },
};

module.exports = {
    data: new SlashCommandBuilder()
        .setName('tienda')
        .setDescription('Compra roles usando tus Nexis')
        .addStringOption(option =>
            option.setName('articulo')
                .setDescription('Elige un artículo para comprar')
                .setRequired(true)
                .addChoices(
                    { name: 'VIP', value: 'vip' },
                    { name: 'Premium', value: 'premium' },
                    { name: 'Elite', value: 'elite' }
                )
        ),
    async execute(client, interaction, profileData) {
        const { id } = interaction.user;
        const articulo = interaction.options.getString('articulo');
        const { balance } = profileData;
        const roleToBuy = roles[articulo];

        if (!roleToBuy) {
            return interaction.reply('Artículo no válido.');
        }

        if (balance < roleToBuy.cost) {
            return interaction.reply(`No tienes suficiente saldo para comprar el rol ${roleToBuy.name}. Necesitas ${roleToBuy.cost} Nexis.`);
        }

        // Obtener el rol del servidor
        const role = interaction.guild.roles.cache.find(r => r.name === roleToBuy.name);
        if (!role) {
            return interaction.reply(`El rol ${roleToBuy.name} no existe en este servidor.`);
        }

        // Añadir el rol al usuario
        const member = interaction.guild.members.cache.get(id);
        await member.roles.add(role);

        // Actualizar el balance del usuario
        await profileSchema.findOneAndUpdate({ userId: id }, { $inc: { balance: -roleToBuy.cost } });

        await interaction.reply(`Has comprado el rol ${roleToBuy.name} por ${roleToBuy.cost} Nexis.`);
    }
};
