const { EmbedBuilder,SlashCommandBuilder } = require('discord.js')
const profileSchema = require('../../../db/models/economy/profileModel')

module.exports = {
    data: new SlashCommandBuilder()
    .setName('transferir')
    .setDescription('Transfiere Nexis a otro usuario')
    .addUserOption(option => option.setName('usuario').setDescription('El usuario al que le transferiras').setRequired(true))
    .addIntegerOption(option => option.setName('cantidad').setDescription('La cantidad de Nexis a transferir').setRequired(true).setMinValue(1)),
    async execute(client, interaction, profileData) {
        const receiveUser = interaction.options.getUser('usuario')
        const amount = interaction.options.getInteger('cantidad')
        const { balance } = profileData;

        if(balance < amount) return interaction.reply({ content: `No tienes suficiente Nexis para transferir ${amount}`})
       const receiveUserData =  await profileSchema.findOneAndUpdate(
    {userId: receiveUser.id},
    {
        $inc: {balance: amount}
    }
)
if(!receiveUserData) return await interaction.reply({content: 'El usuario no esta registrado en el sistema', ephemeral: true})
    await profileSchema.findOneAndUpdate(
        {userId: interaction.user.id},
        {
            $inc: {balance: -amount}
        }
    )
await interaction.reply({ content: `Has transferido ${amount} Nexis a ${receiveUser}`})
    }
}