const { SlashCommandBuilder, ActionRowBuilder, ButtonBuilder, ButtonStyle } = require('discord.js');
const profileSchema = require('../../../db/models/economy/profileModel')
require('dotenv').config();

module.exports = {
    data: new SlashCommandBuilder()
        .setName('ruleta')
        .setDescription('Juega a la Ruleta Rusa y puedes ganar o perder Nexis')
        .addIntegerOption(option => option.setName('apuesta').setDescription('Cantidad a apostar').setRequired(true)),
    async execute(client, interaction, profileData) {
        const { id } = interaction.user;
        const apuesta = interaction.options.getInteger('apuesta');
        const { balance } = profileData;

        // Verifica si el usuario tiene suficiente saldo
        if (balance < apuesta) {
            return interaction.reply(`No tienes suficiente saldo para apostar ${apuesta} Nexis.`);
        }

        // Crear botón para disparar
        const row = new ActionRowBuilder()
            .addComponents(
                new ButtonBuilder()
                    .setCustomId('disparar')
                    .setLabel('Disparar')
                    .setStyle(ButtonStyle.Danger)
            );

        await interaction.reply({
            content: `Has apostado ${apuesta} Nexis. Presiona "Disparar" para jugar a la Ruleta Rusa.`,
            components: [row]
        });

        const filter = i => i.customId === 'disparar' && i.user.id === interaction.user.id;
        const collector = interaction.channel.createMessageComponentCollector({ filter, time: 60000 });

        collector.on('collect', async i => {
            const resultado = Math.random() < 1 / 6; // Simula una bala en un tambor de 6 espacios

            if (resultado) {
                await profileSchema.findOneAndUpdate({ userId: id }, { $inc: { balance: -apuesta } });
                await i.update({ content: `¡Bang! Has perdido ${apuesta} Nexis.`, components: [] });
            } else {
                await profileSchema.findOneAndUpdate({ userId: id }, { $inc: { balance: apuesta } });
                await i.update({ content: `¡Click! Te has salvado y ganado ${apuesta} Nexis.`, components: [] });
            }
            collector.stop();
        });

        collector.on('end', collected => {
            if (collected.size === 0) {
                interaction.editReply({ content: 'No disparaste a tiempo. Juego cancelado.', components: [] });
            }
        });
    }
};
