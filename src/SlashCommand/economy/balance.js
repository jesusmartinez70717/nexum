const { EmbedBuilder,SlashCommandBuilder } = require('discord.js')

module.exports = {
    data: new SlashCommandBuilder()
    .setName('balance')
    .setDescription('Puedes ver tu balance'),
    async execute(client, interaction, profileData) {
        const { balance } = profileData
        const user = interaction.user
        
        const embed = new EmbedBuilder()
        .setColor('Red')
        .setDescription(`${user} Tienes ${balance} Nexis`)

        interaction.reply({embeds: [embed]})
    }
}