const { EmbedBuilder, SlashCommandBuilder } = require('discord.js');
const profileSchema = require('../../../db/models/economy/profileModel')
require('dotenv').config();
const slotSymbols = ['🍒', '🍋', '🍉', '🔔', '⭐'];

module.exports = {
    data: new SlashCommandBuilder()
        .setName('slotmachine')
        .setDescription('Juega en la máquina tragamonedas y puedes ganar o perder Nexis')
        .addIntegerOption(option => option.setName('apuesta').setDescription('Cantidad a apostar').setRequired(true))
        .addStringOption(option => option.setName('eleccion').setDescription('Escoje un símbolo').setRequired(true).addChoices(
            { name: 'Cereza', value: '🍒' },
            { name: 'Limón', value: '🍋' },
            { name: 'Sandía', value: '🍉' },
            { name: 'Campana', value: '🔔' },
            { name: 'Estrella', value: '⭐' }
        )),
    async execute(client, interaction, profileData) {
        const { id } = interaction.user;
        const apuesta = interaction.options.getInteger('apuesta');
        const eleccion = interaction.options.getString('eleccion');
        const { balance } = profileData;

        // Verifica si el usuario tiene suficiente saldo
        if (balance < apuesta) {
            return interaction.reply(`No tienes suficiente saldo para apostar ${apuesta} Nexis.`);
        }

        // Genera los resultados de la tragamonedas
        const result1 = slotSymbols[Math.floor(Math.random() * slotSymbols.length)];
        const result2 = slotSymbols[Math.floor(Math.random() * slotSymbols.length)];
        const result3 = slotSymbols[Math.floor(Math.random() * slotSymbols.length)];

        // Calcula la recompensa en función de los resultados
        let reward = 0;
        let message = `🎰 | ${result1} | ${result2} | ${result3} | `;

        if (result1 === eleccion && result2 === eleccion && result3 === eleccion) {
            reward = apuesta * 5; // Gana 5 veces la apuesta
            message += `¡Felicidades! Has ganado ${reward} Nexis.`;
        } else if ((result1 === eleccion && result2 === eleccion) || 
                   (result1 === eleccion && result3 === eleccion) || 
                   (result2 === eleccion && result3 === eleccion)) {
            reward = apuesta * 2; // Gana 2 veces la apuesta
            message += `¡Felicidades! Has ganado ${reward} Nexis.`;
        } else if (result1 === eleccion || result2 === eleccion || result3 === eleccion) {
            reward = apuesta; // Gana la misma cantidad que apostó
            message += `¡Felicidades! Has ganado ${reward} Nexis.`;
        } else {
            reward = -apuesta; // Pierde la cantidad apostada
            message += `Lo siento, has perdido ${apuesta} Nexis.`;
        }

        // Actualiza el balance del usuario
        await profileSchema.findOneAndUpdate({ userId: id }, { $inc: { balance: reward } });
        await interaction.reply({ content: message });
    }
};
