const { SlashCommandBuilder, ActionRowBuilder, ButtonBuilder, ButtonStyle, EmbedBuilder } = require('discord.js');
const profileSchema = require('../../../db/models/economy/profileModel')
require('dotenv').config();

module.exports = {
    data: new SlashCommandBuilder()
        .setName('carrera')
        .setDescription('Apuesta en una carrera de animales y gana o pierde Nexis')
        .addStringOption(option => 
            option.setName('animal')
                .setDescription('Elige tu animal')
                .setRequired(true)
                .addChoices(
                    { name: 'Tortuga', value: 'tortuga' },
                    { name: 'Liebre', value: 'liebre' },
                    { name: 'Caballo', value: 'caballo' }
                )
        )
        .addIntegerOption(option => option.setName('apuesta').setDescription('Cantidad a apostar').setRequired(true)),
    async execute(client, interaction, profileData) {
        const { id } = interaction.user;
        const animalElegido = interaction.options.getString('animal');
        const apuesta = interaction.options.getInteger('apuesta');
        const { balance } = profileData;

        // Verifica si el usuario tiene suficiente saldo
        if (balance < apuesta) {
            return interaction.reply(`No tienes suficiente saldo para apostar ${apuesta} Nexis.`);
        }

        // Animales participantes
        const animales = ['tortuga', 'liebre', 'caballo'];
        const resultados = {};

        // Simulación de la carrera
        animales.forEach(animal => {
            resultados[animal] = Math.floor(Math.random() * 100) + 1; // Un número aleatorio de 1 a 100
        });

        // Determinar el ganador
        const ganador = Object.keys(resultados).reduce((a, b) => resultados[a] > resultados[b] ? a : b);

        // Crear mensaje del resultado de la carrera
        let mensajeResultado = new EmbedBuilder()
            .setTitle('Resultado de la Carrera de Animales')
            .setDescription(`Resultados:\n${animales.map(animal => `${animal.charAt(0).toUpperCase() + animal.slice(1)}: ${resultados[animal]}`).join('\n')}`)
            .addFields(
                { name: 'Ganador', value: ganador.charAt(0).toUpperCase() + ganador.slice(1), inline: true }
            )
            .setColor('#00FF00')
            .setTimestamp();

        // Actualizar balance del usuario y enviar mensaje final
        if (animalElegido === ganador) {
            await profileSchema.findOneAndUpdate({ userId: id }, { $inc: { balance: apuesta } });
            mensajeResultado.setFooter({ text: `¡Felicidades! Has ganado ${apuesta} Nexis.` });
        } else {
            await profileSchema.findOneAndUpdate({ userId: id }, { $inc: { balance: -apuesta } });
            mensajeResultado.setFooter({ text: `Lo siento, has perdido ${apuesta} Nexis.` });
        }

        await interaction.reply({ embeds: [mensajeResultado] });
    }
};
