const { EmbedBuilder,SlashCommandBuilder, time } = require('discord.js')
const profileSchema = require('../../../db/models/economy/profileModel')
const parseMilliseconds = require('parse-ms-2')
require('dotenv').config()
const coinFlipReward = process.env.COINFLIP_REWARD
module.exports = {
    data: new SlashCommandBuilder()
    .setName('coinflip')
    .setDescription('Lanza un nexi y puedes ganar o perder Nexis')
    .addStringOption(option => option.setName('eleccion').setDescription('Escoje entre cara o cruz').setRequired(true).addChoices(
        {name: 'Cara', value: 'cara'},
        {name: 'Cruz', value: 'cruz'}),
    ),
    async execute(client, interaction, profileData) {
        const { coinflipLastUsed } = profileData
        const { id } = interaction.user;
        const cooldown = 3600000;
        const timeLeft = cooldown - (Date.now() - coinflipLastUsed)
        if(timeLeft > 0){
            const { minutes, seconds } = parseMilliseconds(timeLeft)
            interaction.reply(`Puedes volver a lanzar una moneda dentro de ${minutes} min y ${seconds} seg`)
        }
        await profileSchema.findOneAndUpdate({ userId: id }, { $set: { coinflipLastUsed: Date.now()}})
        const randomNum = Math.round(Math.random());
        const result = randomNum ? 'cara' : 'cruz';
        const choice = interaction.options.getString('eleccion')
        if (choice === result) {
            await profileSchema.findOneAndUpdate({ userId: id }, { $inc: { balance: coinFlipReward}})
            await interaction.reply({ content: `Has ganado ${coinFlipReward} Nexis`})
        } else {
            await profileSchema.findOneAndUpdate({ userId: id }, { $inc: { balance: -1}})
            await interaction.reply({ content: `Has perdido 1 Nexi`})
        }
    }

}