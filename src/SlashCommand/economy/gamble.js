const { SlashCommandBuilder, ActionRowBuilder, ButtonBuilder, ButtonStyle } = require('discord.js');
const profileSchema = require('../../../db/models/economy/profileModel')

module.exports = {
    data: new SlashCommandBuilder()
        .setName('puertas')
        .setDescription('Elige una puerta y puedes ganar, perder o quedar igual')
        .addIntegerOption(option => option.setName('apuesta').setDescription('Cantidad a apostar').setRequired(true)),
    async execute(client, interaction, profileData) {
        const { id } = interaction.user;
        const apuesta = interaction.options.getInteger('apuesta');
        const { balance } = profileData;

        // Verifica si el usuario tiene suficiente saldo
        if (balance < apuesta) {
            return interaction.reply(`No tienes suficiente saldo para apostar ${apuesta} Nexis.`);
        }

        // Crear botones para las puertas
        const row = new ActionRowBuilder()
            .addComponents(
                new ButtonBuilder()
                    .setCustomId('puerta1')
                    .setLabel('Puerta 1')
                    .setStyle(ButtonStyle.Primary),
                new ButtonBuilder()
                    .setCustomId('puerta2')
                    .setLabel('Puerta 2')
                    .setStyle(ButtonStyle.Primary),
                new ButtonBuilder()
                    .setCustomId('puerta3')
                    .setLabel('Puerta 3')
                    .setStyle(ButtonStyle.Primary)
            );

        const message = await interaction.reply({ content: 'Elige una puerta:', components: [row], fetchReply: true });

        // Crear un filtro para las interacciones de los botones
        const filter = i => i.user.id === interaction.user.id;

        // Esperar la interacción de los botones
        const collector = message.createMessageComponentCollector({ filter, time: 60000 });

        collector.on('collect', async i => {
            if (!i.isButton()) return;

            // Determinar el resultado de la puerta seleccionada
            const doorResults = ['ganar', 'perder', 'nada'];
            const shuffledResults = doorResults.sort(() => Math.random() - 0.5);
            const selectedDoor = i.customId;

            let result;
            if (selectedDoor === 'puerta1') {
                result = shuffledResults[0];
            } else if (selectedDoor === 'puerta2') {
                result = shuffledResults[1];
            } else if (selectedDoor === 'puerta3') {
                result = shuffledResults[2];
            }

            let messageContent;
            if (result === 'ganar') {
                await profileSchema.findOneAndUpdate({ userId: id }, { $inc: { balance: apuesta } });
                messageContent = `¡Felicidades! Has ganado ${apuesta} Nexis.`;
            } else if (result === 'perder') {
                await profileSchema.findOneAndUpdate({ userId: id }, { $inc: { balance: -apuesta } });
                messageContent = `Lo siento, has perdido ${apuesta} Nexis.`;
            } else {
                messageContent = `No has ganado ni perdido Nexis.`;
            }

            await i.update({ content: messageContent, components: [] });
        });

        collector.on('end', collected => {
            if (collected.size === 0) {
                interaction.editReply({ content: 'No elegiste ninguna puerta a tiempo.', components: [] });
            }
        });
    }
};
