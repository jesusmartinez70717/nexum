const { EmbedBuilder, SlashCommandBuilder, ActionRowBuilder, ButtonBuilder, ButtonStyle } = require('discord.js');
const profileSchema = require('../../../db/models/economy/profileModel')
require('dotenv').config();

function drawCard() {
    const values = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
    const suits = ['♠', '♣', '♥', '♦'];
    const value = values[Math.floor(Math.random() * values.length)];
    const suit = suits[Math.floor(Math.random() * suits.length)];
    return { value, suit };
}

function calculateHandValue(hand) {
    let value = 0;
    let aces = 0;

    hand.forEach(card => {
        if (card.value === 'A') {
            aces += 1;
            value += 11;
        } else if (['K', 'Q', 'J'].includes(card.value)) {
            value += 10;
        } else {
            value += parseInt(card.value, 10);
        }
    });

    while (value > 21 && aces > 0) {
        value -= 10;
        aces -= 1;
    }

    return value;
}

function handToString(hand) {
    return hand.map(card => `${card.value}${card.suit}`).join(' ');
}

module.exports = {
    data: new SlashCommandBuilder()
        .setName('blackjack')
        .setDescription('Juega al Blackjack y puedes ganar o perder Nexis')
        .addIntegerOption(option => option.setName('apuesta').setDescription('Cantidad a apostar').setRequired(true)),
    async execute(client, interaction, profileData) {
        const { id } = interaction.user;
        const apuesta = interaction.options.getInteger('apuesta');
        const { balance } = profileData;

        // Verifica si el usuario tiene suficiente saldo
        if (balance < apuesta) {
            return interaction.reply(`No tienes suficiente saldo para apostar ${apuesta} Nexis.`);
        }

        // Mano del jugador y de la banca
        let playerHand = [drawCard(), drawCard()];
        let dealerHand = [drawCard(), drawCard()];

        // Turno del jugador
        let playerValue = calculateHandValue(playerHand);
        let dealerValue = calculateHandValue(dealerHand);

        const row = new ActionRowBuilder()
            .addComponents(
                new ButtonBuilder()
                    .setCustomId('hit')
                    .setLabel('Hit')
                    .setStyle(ButtonStyle.Primary),
                new ButtonBuilder()
                    .setCustomId('stand')
                    .setLabel('Stand')
                    .setStyle(ButtonStyle.Primary)
            );

        await interaction.reply({
            content: `Tu mano: ${handToString(playerHand)} (Valor: ${playerValue})\nCarta visible de la banca: ${dealerHand[0].value}${dealerHand[0].suit}`,
            components: [row]
        });

        const filter = i => i.user.id === interaction.user.id;

        const collector = interaction.channel.createMessageComponentCollector({ filter, time: 60000 });

        collector.on('collect', async i => {
            if (i.customId === 'hit') {
                playerHand.push(drawCard());
                playerValue = calculateHandValue(playerHand);

                if (playerValue > 21) {
                    await profileSchema.findOneAndUpdate({ userId: id }, { $inc: { balance: -apuesta } });
                    await i.update({
                        content: `Tu mano: ${handToString(playerHand)} (Valor: ${playerValue})\nTe pasaste de 21. Has perdido ${apuesta} Nexis.`,
                        components: []
                    });
                    collector.stop();
                } else {
                    await i.update({
                        content: `Tu mano: ${handToString(playerHand)} (Valor: ${playerValue})\nCarta visible de la banca: ${dealerHand[0].value}${dealerHand[0].suit}`,
                        components: [row]
                    });
                }
            } else if (i.customId === 'stand') {
                collector.stop();

                while (dealerValue < 17) {
                    dealerHand.push(drawCard());
                    dealerValue = calculateHandValue(dealerHand);
                }

                let message = `Tu mano: ${handToString(playerHand)} (Valor: ${playerValue})\nMano de la banca: ${handToString(dealerHand)} (Valor: ${dealerValue})\n`;

                if (dealerValue > 21 || playerValue > dealerValue) {
                    await profileSchema.findOneAndUpdate({ userId: id }, { $inc: { balance: apuesta } });
                    message += `¡Ganaste! Has ganado ${apuesta} Nexis.`;
                } else if (playerValue < dealerValue) {
                    await profileSchema.findOneAndUpdate({ userId: id }, { $inc: { balance: -apuesta } });
                    message += `La banca gana. Has perdido ${apuesta} Nexis.`;
                } else {
                    message += `Empate. No has ganado ni perdido Nexis.`;
                }

                await i.update({ content: message, components: [] });
            }
        });

        collector.on('end', collected => {
            if (collected.size === 0) {
                interaction.editReply({ content: 'No elegiste ninguna opción a tiempo. Juego cancelado.', components: [] });
            }
        });
    }
};
