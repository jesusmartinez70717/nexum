const { SlashCommandBuilder, EmbedBuilder, Routes, DataResolver } = require('discord.js');

module.exports = {
    owner: true,
    data: new SlashCommandBuilder()
    .setName('bot-banner')
    .setDescription('Configura el banner del bot')
    .addAttachmentOption(option => option.setName('banner').setDescription('El banner que se le pondra al bot').setRequired(true)),
    async execute(client, interaction) {
        const banner = interaction.options.getAttachment('banner');

        async function sendMessage(message) {
            const embed = new EmbedBuilder()
            .setColor('Red')
            .setDescription(message)
            interaction.reply({ embeds: [embed], ephemeral: true });
        }
        if(banner.contentType !== "image/gif" && banner.contentType !== "image/png") return await sendMessage('El banner debe ser en formato gif o png')
        
        let error; 
        await client.rest.patch(Routes.user(), {
            body: {banner: await DataResolver.resolveImage(banner.url)}
        }).catch(async err => {
            error: true
            await sendMessage(`Error: ${err.toString()}`)
        })
        if(error) return;

        await sendMessage(`El banner fue configurado correctamente`)
    }
}