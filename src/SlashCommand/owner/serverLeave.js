const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');

module.exports = {
  owner: true,
  data: new SlashCommandBuilder()
    .setName('server-leave')
    .setDescription('El bot se sale del servidor especificado')
    .addStringOption(option => 
      option
        .setName('id')
        .setDescription('El ID del servidor del que el bot debe salir')
        .setRequired(true)
    ),
  async execute(client, interaction) {
    const guildId = interaction.options.getString('id');
    const guild = client.guilds.cache.get(guildId);
    if (!guild) {
      return interaction.reply({ content: 'No se encontró el servidor', ephemeral: true });
    }

    const embed = new EmbedBuilder()
      .setColor('#FF0000')
      .setTitle('Confirmación de salida del servidor')
      .setDescription(`El bot ha salido del servidor **${guild.name}**`)
      .addFields(
        { name: 'Nombre del Servidor', value: guild.name, inline: true },
        { name: 'ID del Servidor', value: guild.id, inline: true },
        { name: 'Número de Miembros', value: guild.memberCount.toString(), inline: true }
      )
      .setTimestamp();

    await interaction.reply({ embeds: [embed], ephemeral: true });
    await guild.leave();
  }
};
