const { SlashCommandBuilder } = require('@discordjs/builders');
const { joinVoiceChannel, createAudioResource, VoiceConnectionStatus } = require('@discordjs/voice');
const { EmbedBuilder } = require('discord.js');
const ytdl = require('ytdl-core');
const ytSearch = require('yt-search');
const playerState = require('../../../services/music/playerState.js');

async function searchYouTube(query) {
    const result = await ytSearch(query);
    if (result && result.videos.length > 0) {
        return result.videos[0].url;
    } else {
        throw new Error('No se encontraron resultados para la consulta.');
    }
}

async function getYouTubeInfo(url) {
    const info = await ytdl.getInfo(url);
    return {
        title: info.videoDetails.title,
        url: info.videoDetails.video_url,
        thumbnail: info.videoDetails.thumbnails[0].url,
        author: info.videoDetails.author.name,
        lengthSeconds: info.videoDetails.lengthSeconds
    };
}

module.exports = {
    data: new SlashCommandBuilder()
        .setName('playlistadd')
        .setDescription('Agrega una canción a la lista de reproducción desde YouTube')
        .addStringOption(option => option.setName('query').setDescription('La URL o consulta de YouTube').setRequired(true)),
    async execute(client, interaction) {
        await interaction.deferReply();

        let query = interaction.options.getString('query');
        let url = query;

        const voiceChannel = interaction.member.voice.channel;
        if (!voiceChannel) {
            return interaction.editReply({ content: 'Debes estar en un canal de voz para usar este comando.', ephemeral: true });
        }

        if (!ytdl.validateURL(url)) {
            try {
                url = await searchYouTube(query);
            } catch (error) {
                return interaction.editReply({ content: `Error: ${error.message}`, ephemeral: true });
            }
        }

        const songInfo = await getYouTubeInfo(url);

        const stream = ytdl(url, { filter: 'audioonly' });
        const resource = createAudioResource(stream, { inlineVolume: true });
        resource.metadata = {
            title: songInfo.title,
            author: songInfo.author,
            lengthSeconds: songInfo.lengthSeconds,
            thumbnail: songInfo.thumbnail
        };

        // Agregar el recurso a la lista de reproducción
        playerState.playlist.push(resource);

        // Verificar si el reproductor está creado y la conexión de voz está establecida
        if (!playerState.currentPlayer || !playerState.voiceConnection) {
            playerState.createPlayer();

            // Si no hay conexión de voz, crearla y subscribir al reproductor
            if (!playerState.voiceConnection) {
                playerState.voiceConnection = joinVoiceChannel({
                    channelId: voiceChannel.id,
                    guildId: interaction.guild.id,
                    adapterCreator: interaction.guild.voiceAdapterCreator,
                });

                playerState.voiceConnection.on(VoiceConnectionStatus.Disconnected, () => {
                    playerState.handleError();
                });

                playerState.voiceConnection.subscribe(playerState.currentPlayer);
            }

            // Reproducir la siguiente canción en la lista de reproducción
            playerState.playNext();
        }

        const embed = new EmbedBuilder()
            .setColor('#0099ff')
            .setTitle(songInfo.title)
            .setURL(songInfo.url)
            .setAuthor({name: songInfo.author, iconURL: songInfo.thumbnail, url: songInfo.url})
            .setDescription(`Duración: ${Math.floor(songInfo.lengthSeconds / 60)}:${songInfo.lengthSeconds % 60}`)
            .setThumbnail(songInfo.thumbnail)
            .setTimestamp();

        await interaction.editReply({ embeds: [embed] });
    }
};
