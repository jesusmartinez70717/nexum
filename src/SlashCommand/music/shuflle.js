const { SlashCommandBuilder } = require('@discordjs/builders');
const playerState = require('../../../services/music/playerState.js');

function shuffle(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}

module.exports = {
    data: new SlashCommandBuilder()
        .setName('shuffle')
        .setDescription('Baraja aleatoriamente la lista de reproducción actual'),
    async execute(client, interaction) {
        if (playerState.playlist.length > 1) {
            playerState.playlist = shuffle(playerState.playlist);
            await interaction.reply({ content: 'Lista de reproducción barajada aleatoriamente.' });
        } else {
            await interaction.reply({ content: 'No hay suficientes canciones en la lista de reproducción para barajar.', ephemeral: true });
        }
    }
};
