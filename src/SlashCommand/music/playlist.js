const { SlashCommandBuilder } = require('@discordjs/builders');
const { EmbedBuilder } = require('discord.js');
const playerState = require('../../../services/music/playerState.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('playlist')
        .setDescription('Muestra las canciones en la lista de reproducción actual'),
    async execute(client, interaction) {
        const playlist = playerState.getPlaylist();
        if (playlist.length > 0) {
            const embed = new EmbedBuilder()
                .setColor('#0099ff')
                .setTitle('Lista de Reproducción Actual')
                .setDescription('Estas son las canciones actualmente en la lista de reproducción:')
                .setTimestamp();

            playlist.forEach((song, index) => {
                embed.addFields({name: `Canción ${index + 1}`, value: song, inline: false});
            });

            await interaction.reply({ embeds: [embed] });
        } else {
            await interaction.reply({ content: 'No hay canciones en la lista de reproducción actualmente.', ephemeral: true });
        }
    }
};
