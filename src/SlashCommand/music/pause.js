const { SlashCommandBuilder } = require('@discordjs/builders');
const playerState = require('../../../services/music/playerState.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('pause')
        .setDescription('Pausa la reproducción de la canción actual'),
    async execute(client, interaction) {
        if (playerState.currentPlayer && playerState.currentPlayer.state.status === 'playing') {
            playerState.currentPlayer.pause();
            await interaction.reply({ content: 'Reproducción pausada.' });
        } else {
            await interaction.reply({ content: 'No hay ninguna canción reproduciéndose en este momento.', ephemeral: true });
        }
    }
};
