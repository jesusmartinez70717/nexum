const { SlashCommandBuilder } = require('@discordjs/builders');
const playerState = require('../../../services/music/playerState.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('volume')
        .setDescription('Ajusta el volumen de la canción actual')
        .addIntegerOption(option =>
            option.setName('amount')
                .setDescription('El nivel de volumen (0-100)')
                .setRequired(true)
                .setMinValue(0)
                .setMaxValue(100)
        ),
    async execute(client, interaction) {
        const volume = interaction.options.getInteger('amount') / 100;

        if (playerState.currentPlayer && playerState.currentResource) {
            playerState.setVolume(volume);
            await interaction.reply({ content: `Volumen ajustado a ${volume * 100}%` });
        } else {
            await interaction.reply({ content: 'No hay ninguna canción reproduciéndose en este momento.', ephemeral: true });
        }
    }
};
