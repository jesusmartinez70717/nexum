const { SlashCommandBuilder } = require('@discordjs/builders');
const playerState = require('../../../services/music/playerState.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('stop')
        .setDescription('Detiene la reproducción actual y limpia la lista de reproducción'),
    async execute(client, interaction) {
        if (playerState.currentPlayer) {
            playerState.currentPlayer.stop();
            playerState.playlist = [];
            await interaction.reply({ content: 'Reproducción detenida y lista de reproducción limpia.' });
        } else {
            await interaction.reply({ content: 'No hay ninguna canción reproduciéndose para detener.', ephemeral: true });
        }
    }
};
