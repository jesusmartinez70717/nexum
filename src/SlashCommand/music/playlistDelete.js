const { SlashCommandBuilder } = require('@discordjs/builders');
const playerState = require('../../../services/music/playerState.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('playlistdelete')
        .setDescription('Elimina una canción de la lista de reproducción')
        .addStringOption(option => option.setName('query').setDescription('Índice o nombre de la canción a eliminar').setRequired(true)),
    async execute(client, interaction) {
        const query = interaction.options.getString('query');

        let removed = false;
        if (!isNaN(query)) {
            // If query is a number, assume it's an index
            const index = parseInt(query + 1 , 10);
            removed = playerState.removeFromPlaylist(index);
        } else {
            // Otherwise, assume it's a name
            removed = playerState.removeFromPlaylist(query);
        }

        if (removed) {
            await interaction.reply({ content: 'Canción eliminada de la lista de reproducción.' });
        } else {
            await interaction.reply({ content: 'No se encontró ninguna canción para eliminar.', ephemeral: true });
        }
    }
};
