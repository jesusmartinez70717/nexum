const { SlashCommandBuilder } = require('@discordjs/builders');
const playerState = require('../../../services/music/playerState.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('nowplaying')
        .setDescription('Muestra la canción actualmente en reproducción'),
    async execute(client, interaction) {
        if (playerState.currentResource && playerState.currentResource.metadata) {
            const title = playerState.currentResource.metadata.title || 'Canción desconocida';
            await interaction.reply({ content: `Reproduciendo ahora: ${title}` });
        } else {
            await interaction.reply({ content: 'No hay ninguna canción reproduciéndose actualmente.', ephemeral: true });
        }
    }
};
