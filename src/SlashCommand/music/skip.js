const { SlashCommandBuilder } = require('@discordjs/builders');
const { EmbedBuilder } = require('discord.js');
const playerState = require('../../../services/music/playerState.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('skip')
        .setDescription('Salta la canción actual en reproducción'),
    async execute(client, interaction) {
        if (playerState.currentPlayer && playerState.currentPlayer.state.status === 'playing') {
            const currentSong = playerState.currentResource.metadata;

            playerState.currentPlayer.stop();

            const embed = new EmbedBuilder()
                .setColor('#ff0000')
                .setTitle('Canción saltada')
                .setDescription(`**Título:** ${currentSong.title}\n**Artista:** ${currentSong.author}\n**Duración:** ${Math.floor(currentSong.lengthSeconds / 60)}:${currentSong.lengthSeconds % 60}`)
                .setThumbnail(currentSong.thumbnail)
                .setTimestamp();

            await interaction.reply({ embeds: [embed] });
        } else {
            await interaction.reply({ content: 'No hay ninguna canción reproduciéndose para saltar.', ephemeral: true });
        }
    }
};
