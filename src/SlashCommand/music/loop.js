const { SlashCommandBuilder } = require('@discordjs/builders');
const playerState = require('../../../services/music/playerState.js');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('loop')
        .setDescription('Activa o desactiva el modo de repetición de la canción o lista de reproducción'),
    async execute(client, interaction) {
        const loopEnabled = playerState.toggleLoop();

        if (loopEnabled) {
            await interaction.reply({ content: 'Modo de repetición activado.', ephemeral: true });
        } else {
            await interaction.reply({ content: 'Modo de repetición desactivado.', ephemeral: true });
        }
    }
};
